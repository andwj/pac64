;===============================================
;
;     PACMAN CLONE for the C=64
;
;     by Andrew Apted, July 2019
;
;===============================================


; VIC-II hardware registers

.alias	VIC_CTL1	$D011	; yscroll, bitmap mode, blanking
.alias	VIC_CTL2	$D016	; xscroll, etc
.alias	VIC_PTRS	$D018	; pointers for screen and chars
.alias	VIC_RASTER	$D012	; low 8 bits of raster (scanline)
.alias	VIC_INT_QRY	$D019	; interrupt query
.alias	VIC_INT_ENA	$D01A	; interrupt enable
.alias	VIC_EXT_COL	$D020	; exterior (border) color
.alias	VIC_BG_COL	$D021	; background colors (+0..+3)

.alias	VIC_SP_X	$D000	; + N * 2
.alias	VIC_SP_Y	$D001	; + N * 2
.alias	VIC_SP_XMSB	$D010	; most significant bit of X
.alias	VIC_SP_ENA	$D015	; sprite enable
.alias	VIC_SP_COL	$D027	; sprite colors (+ N)
.alias	VIC_SP_MCM	$D01C	; sprite multi-color enable
.alias	VIC_SP_MC1	$D025	; sprite multi-color #1
.alias	VIC_SP_MC2	$D026	; sprite multi-color #2

.alias	VIC_SP_XEX	$D01D	; sprite X expand
.alias	VIC_SP_YEX	$D017	; sprite Y expand
.alias	VIC_SP_PRI	$D01B	; sprite priority to background
.alias	VIC_HIT_SP	$D01E	; sprite-to-sprite collision
.alias	VIC_HIT_BG	$D01F	; sprite-to-background collision


V_InitVideo:
	; use second bank (at addr $4000), which means the CHARGEN
	; rom is not visible to the VIC (hence usable by us).
	lda	#BANK_4000
	sta	CIA2_PRA

	; reset all registers to zero
	lda	#0
	ldx	#0
*
	sta	$D000,x
	inx
	bne	-

	; set these flags in CTL1:
	;    ECM (extended color mode) to 0
	;    BMM (bitmask mode) to 0
	;    DEN (display enable) to 1
	;    RSEL (row select) to 1 (give 25 screen rows)
	;    YSCROLL to 3 (normal for 25 rows)
	lda	#$1B
	sta	VIC_CTL1

	; set these flags in CTL2:
	;    MCM (multi-color backgrounds) to 0
	;    CSEL (column select) to 1 (give 40 screen columns)
	;    XSCROLL to 0 (normal for 40 columns)
	lda	#$08
	sta	VIC_CTL2

	; set memory pointers:
	;   VM (video memory)   to $c --> $7000
	;   CB (character base) to $e --> $7800
	lda	#$ce
	sta	VIC_PTRS

	; black background and border
	lda	#C_BLACK
	sta	VIC_BG_COL
	sta	VIC_BG_COL+1
	sta	VIC_BG_COL+2
	sta	VIC_BG_COL+3
	sta	VIC_EXT_COL

	; copy character set
	`stor16	charset_data,R1
	`stor16	2048,R2
	`stor16	charset_p,R3
	jsr	U_CopyMem

	; copy sprite data
	`stor16	sprite_data,R1
	`stor16	2048,R2
	`stor16	sprites_p,R3
	jsr	U_CopyMem

	rts


V_ClearScreen:
	; clear screen
	`stor16	screen_p,R1
	`stor16	1024,R2
	lda	#0
	jsr	U_FillMem

	; clear colors
	`stor16	colors_p,R1
	`stor16	1024,R2
	lda	#C_MGRAY
	jsr	U_FillMem
	rts


V_ClearHeader:
	ldx	#80
	lda	#0
*
	sta	screen_p,x
	dex
	bpl	-
	rts


V_DrawTitle:
	jsr	V_ClearScreen

	; draw title text: "PACCIE 64"
	`stor16	screen_p+160,R6
	`stor16	colors_p+160,R5

	lda	#C_YELLOW
	sta	MAZE_2
	ldy	#11

	lda	#160
	jsr	V_DrawBigChar_Step
	lda	#164
	jsr	V_DrawBigChar_Step
	lda	#168
	jsr	V_DrawBigChar_Step
	lda	#168
	jsr	V_DrawBigChar_Step
	lda	#172
	jsr	V_DrawBigChar_Step
	lda	#176
	jsr	V_DrawBigChar_Step

	lda	#DOT_COL
	sta	MAZE_2
	lda	#144
	jsr	V_DrawBigChar_Step

	lda	#C_LBLUE
	sta	MAZE_2
	lda	#224
	jsr	V_DrawBigChar_Step
	lda	#216
	jsr	V_DrawBigChar_Step

	; press FIRE message
	ldx	#18
*
	lda	author_message,x
	sta	screen_p+40*8+12,x
	lda	#C_MGRAY
	sta	colors_p+40*8+12,x

	lda	press_fire_message,x
	sta	screen_p+40*16+11,x
	lda	#C_WHITE
	sta	colors_p+40*16+11,x

	dex
	bpl	-

	rts


author_message:
.byte	"By Andrew Apted    "

press_fire_message:
.byte	"PRESS FIRE TO START"

game_over_message:
.byte	"GAME OVER"


V_DrawMaze:
	`stor16	0,R8	; y
	`stor16	screen_p+120,R6
	`stor16	colors_p+120,R5
	`stor16	maze_map+40,R4

	; R8+1 is row containing the pellets
	lda	LEVEL
	and	#$0F  ; low digit
	tay

	lda	pellet_rows,y
	lsr
	lsr
	sta	R8+1
*
	jsr	V_DrawMazeLine

	`add16	40,R6
	`add16	40,R5
	`add16	40,R4

	inc	R8
	lda	R8
	cmp	#22
	bne	-

	rts


V_DrawMazeLine:
	ldy	#39
*
	jsr	V_DrawMazeChar
	jsr	V_AddMazeItem

	dey
	bpl	-

	rts


V_DrawMazeChar:
	; Y = x coordinate (0 to 39)
	; R8 = y coordinate (0 to 21)
	; R6 = start of screen line
	; R5 = start of colors
	; R4 = start of maze line

	lda	#0
	sta	MAZE_1

	lda	(R4),y
	cmp	#35	; '#
	beq	+

	; not a wall, so leave it blank
	rts
*
	; left join?
	cpy	#0
	beq	+

	dey
	lda	(R4),y
	iny

	cmp	#35	; '#
	bne	+

	lda	MAZE_1
	ora	#LF
	sta	MAZE_1
*
	; right join?
	iny
	lda	(R4),y
	dey

	cmp	#35	; '#
	bne	+

	lda	MAZE_1
	ora	#RT
	sta	MAZE_1
*
	; bottom join?
	`add16	40,R4
	lda	(R4),y
	pha
	`sub16	40,R4
	pla

	cmp	#35	; '#
	bne	+

	lda	MAZE_1
	ora	#DN
	sta	MAZE_1
*
	; top join?
	`sub16	40,R4
	lda	(R4),y
	pha
	`add16	40,R4
	pla

	cmp	#35	; '#
	bne	+

	lda	MAZE_1
	ora	#UP
	sta	MAZE_1
*
	; compute maze character
	lda	MAZE_1
	ora	#128
	sta	(R6),y

	lda	#C_BLUE
	sta	(R5),y

	rts


V_AddMazeItem:
	; Y = x coordinate (0 to 39)
	; R8 = y coordinate (0 to 21)
	; R6 = start of screen line
	; R5 = start of colors
	; R4 = start of maze line

	lda	#0
	sta	MAZE_1

	lda	(R4),y
	cmp	#'d
	bne	+

	jmp	V_AddDotVert
*
	cmp	#'e
	bne	+

	jmp	V_AddDotHoriz
*
	cmp	#'f
	bne	+

	jmp	V_AddDotFull
*
	cmp	#'P
	bne	++

	; only convert 'P' in a single row
	lda	R8
	cmp	R8+1
	bne	+
	jmp	V_AddPellet
*
	jmp	V_AddDotFull

	; unknown item
*
	rts


.alias	DOT_COL	C_ORANGE
.alias	PELLET_COL	C_LBLUE


V_AddDotVert:
	inc	DOTNUM

	lda	#148
	sta	(R6),y
	lda	#DOT_COL
	sta	(R5),y

	`add16	40,R6
	`add16	40,R5

	lda	#149
	sta	(R6),y
	lda	#DOT_COL
	sta	(R5),y

	`sub16	40,R6
	`sub16	40,R5

	rts


V_AddDotHoriz:
	inc	DOTNUM

	lda	#150
	sta	(R6),y
	lda	#DOT_COL
	sta	(R5),y

	iny
	sta	(R5),y
	lda	#151
	sta	(R6),y
	dey

	rts


V_AddDotFull:
	inc	DOTNUM

	lda	#DOT_COL
	sta	MAZE_2
	lda	#144
	jmp	V_DrawBigChar


V_AddPellet:
	inc	DOTNUM

	lda	#PELLET_COL
	sta	MAZE_2
	lda	#152
	jmp	V_DrawBigChar


V_CheckDotOrPellet:
	; R1 is X coordinate of Paccie
	; R2 is Y coordinate
	; output: R3 = 0 if nothing, 1 = dot, 2 = pellet
	; clobbers: Y, A

	; offset by a single screen cell, since Paccie is 2x2 cells
	; and we want the cell closest to the middle of Paccie.
	; we ALSO want to round to nearest, so add 1/2 a cell too.
	`add16	1023,R1
	`add16	1023,R2

	`lsr16	R1
	`lsr16	R1

	`lsr16	R2
	`lsr16	R2

	lda	#0
	sta	R3 ; result

	; compute: screen_p + Y*40 + X
	lda	R2+1
	asl
	tay

	lda	multiply_by_40,y
	sta	R2
	lda	multiply_by_40+1,y
	sta	R2+1

	`add16	screen_p+120,R2

	ldy	R1+1
	lda	(R2),y

	; need a char between 144 and 156
	cmp	#144
	bcs	+
	rts
*
	cmp	#156
	bcc	+
	rts
*
	inc	R3

	; is it a pellet?
	cmp	#152
	bcc	+
	inc	R3
*
	; remove the item from the screen
	dec	DOTNUM

	; first determine upper/left corner
	sec
	sbc	#144
	tay

	lda	item_sizes_by_char,y
	bpl	+

	`sub16	40,R2  ; decrement Y
*
	lda	item_sizes_by_char,y
	sta	R3+1
	asl
	bpl	+

	dec	R1+1  ; decrement X
*
	ldy	R1+1
	jsr	V_EraseDotPart

	lda	R3+1
	and	#2
	bne	+
	rts
*
	`add16	40,R2

V_EraseDotPart:
	lda	#0
	sta	(R2),y

	lda	R3+1
	and	#1
	beq	+

	iny
	lda	#0
	sta	(R2),y
	dey
*
	rts


.alias	TOUCH_DIST	4

V_IsTouching:
	; checks if actor is in chomping dist to Paccie
	; X = actor  e.g. GHOST1, BONUS
	; result: C set if close enough
	; clobbers: A, R2, R3

	; R2 = ACTOR.X - PAC.X
	lda	PAC+_X+1
	lsr
	sta	R2

	lda	_X+1,x
	lsr
	sec
	sbc	R2

	; if R2 < 0, R2 = -R2
	bpl	+
	`negate
*
	sta	R2

	; early out if large X distance
	cmp	#TOUCH_DIST*2
	bcc	+
	clc
	rts
*
	; R3 = ACTOR.Y - PAC.Y
	lda	PAC+_Y+1
	lsr
	sta	R3

	lda	_Y+1,x
	lsr
	sec
	sbc	R3

	; if R3 < 0, R3 = -R3
	bpl	+
	`negate
*
	sta	R3

	; early out if large Y distance
	cmp	#TOUCH_DIST*2
	bcc	+
	clc
	rts
*
	; approximate calc for 2D distance
	; if R2 >= R3
	;    dist = R2 + R3/2
	; else
	;    dist = R3 + R2/2
	lda	R2
	cmp	R3
	bcc	V_IsTouching_Vert

V_IsTouching_Horiz:
	lda	R3
	lsr
	clc
	adc	R2

	; is distance < TOUCH_DIST ?
	cmp	#TOUCH_DIST
	bcc	+
	clc
	rts
*
	sec
	rts

V_IsTouching_Vert:
	lda	R2
	lsr
	clc
	adc	R3

	; is distance < TOUCH_DIST ?
	cmp	#TOUCH_DIST
	bcc	+

	clc
	rts
*
	sec
	rts


;-----------------------------------------------------------------------

; NOTE: the maze map has an unused row at the very top and bottom
; which must be blank.
maze_map:
.byte	"                                        "
.byte	"        #########################       "
.byte	"        #P df df df df df df dP #       "
.byte	"        #                       #       "
.byte	"     ####e ########   ########e ####    "
.byte	"     #f df #P df df df df dP #f df #    "
.byte	"     #     #                 #     #    "
.byte	"     #e #e #e #e #######e #e #e #e #    "
.byte	"     #f #P df #f #     #f #f dP #f #    "
.byte	"     #  #     #  #     #  #     #  #    "
.byte	"     #e #######e ##   ##e #######e #    "
.byte	"     #P df #f df df df df df #f dP #    "
.byte	"     #     #                 #     #    "
.byte	"     #e #e #e #############e #e #e #    "
.byte	"     #f #f df df df df df df df #f #    "
.byte	"     #  #                       #  #    "
.byte	"     #e #e ########   ########e #e #    "
.byte	"     #f dP #f df df df df df #P df #    "
.byte	"     #     #                 #     #    "
.byte	"     #e ####e #e #######e #e ####e #    "
.byte	"     #f df dP #f df df df #P df df #    "
.byte	"     #        #           #        #    "
.byte	"     ###############################    "
.byte	"                                       "

; travel X coordinates (*4)
.alias	TX_1	6*4
.alias	TX_2	9*4
.alias	TX_3	12*4
.alias	TX_4	15*4
.alias	TX_5	78  ; MIDDLE
.alias	TX_6	24*4
.alias	TX_7	27*4
.alias	TX_8	30*4
.alias	TX_9	33*4

; travel Y coordinates (*4)
.alias	TY_1	1*4
.alias	TY_2	4*4
.alias	TY_3	7*4  ; GHOST BOX
.alias	TY_4	10*4
.alias	TY_5	13*4
.alias	TY_6	16*4
.alias	TY_7	19*4


.alias	pac_start_tx	TX_5
.alias	pac_start_ty	TY_5+6

.alias	bon_start_tx	TX_5
.alias	bon_start_ty	TY_1+6

.alias	ghost_pen_tx	TX_5
.alias	ghost_pen_ty	TY_3


pellet_rows:
.byte	TY_1, TY_3, TY_6, TY_2, TY_4
.byte	TY_7, TY_6, TY_3, TY_4, TY_2


; a NODE is a point in the maze where a ghost or the player
; may change direction.  Ghosts and players are always
; travelling between one node and a neighbor.
;
; each NODE contains the coordinate (Y and X, one byte each) and
; four pointers to neighbor nodes (which are zero next to walls).
; the order of the pointers is: left, right, down, up.

.macro	node	; y, x, left, right, down, up
	.byte	_1,_2
	.word	_3,_4,_5,_6
.macend

node_12:	`node	TY_1,TX_2, 0      ,node_15,node_22,0
node_15:	`node	TY_1,TX_5, node_12,node_18,node_25,0
node_18:	`node	TY_1,TX_8, node_15,0      ,node_28,0
node_21:	`node	TY_2,TX_1, 0      ,node_22,node_41,0
node_22:	`node	TY_2,TX_2, node_21,0      ,node_32,node_12
node_23:	`node	TY_2,TX_3, 0      ,node_24,node_33,0
node_24:	`node	TY_2,TX_4, node_23,node_25,node_44,0
node_25:	`node	TY_2,TX_5, node_24,node_26,0      ,node_15
node_26:	`node	TY_2,TX_6, node_25,node_27,node_46,0
node_27:	`node	TY_2,TX_7, node_26,0      ,node_37,0
node_28:	`node	TY_2,TX_8, 0      ,node_29,node_38,node_18
node_29:	`node	TY_2,TX_9, node_28,0      ,node_49,0
node_32:	`node	TY_3,TX_2, 0      ,node_33,0      ,node_22
node_33:	`node	TY_3,TX_3, node_32,0      ,0      ,node_23
node_35:	`node	TY_3,TX_5, 0      ,0      ,node_45,0
node_37:	`node	TY_3,TX_7, 0      ,node_38,0      ,node_27
node_38:	`node	TY_3,TX_8, node_37,0      ,0      ,node_28
node_41:	`node	TY_4,TX_1, 0      ,node_42,node_61,node_21
node_42:	`node	TY_4,TX_2, node_41,0      ,node_52,0
node_43:	`node	TY_4,TX_3, 0      ,node_44,node_53,0
node_44:	`node	TY_4,TX_4, node_43,node_45,0      ,node_24
node_45:	`node	TY_4,TX_5, node_44,node_46,0      ,node_35
node_46:	`node	TY_4,TX_6, node_45,node_47,0      ,node_26
node_47:	`node	TY_4,TX_7, node_46,0      ,node_57,0
node_48:	`node	TY_4,TX_8, 0      ,node_49,node_58,0
node_49:	`node	TY_4,TX_9, node_48,0      ,node_69,node_29
node_52:	`node	TY_5,TX_2, 0      ,node_53,node_62,node_42
node_53:	`node	TY_5,TX_3, node_52,node_55,0      ,node_43
node_55:	`node	TY_5,TX_5, node_53,node_57,node_65,0
node_57:	`node	TY_5,TX_7, node_55,node_58,0      ,node_47
node_58:	`node	TY_5,TX_8, node_57,0      ,node_68,node_48
node_61:	`node	TY_6,TX_1, 0      ,node_62,node_71,node_41
node_62:	`node	TY_6,TX_2, node_61,0      ,0      ,node_52
node_63:	`node	TY_6,TX_3, 0      ,node_64,node_73,0
node_64:	`node	TY_6,TX_4, node_63,node_65,node_74,0
node_65:	`node	TY_6,TX_5, node_64,node_66,0      ,node_55
node_66:	`node	TY_6,TX_6, node_65,node_67,node_76,0
node_67:	`node	TY_6,TX_7, node_66,0      ,node_77,0
node_68:	`node	TY_6,TX_8, 0      ,node_69,0      ,node_58
node_69:	`node	TY_6,TX_9, node_68,0      ,node_79,node_49
node_71:	`node	TY_7,TX_1, 0      ,node_73,0      ,node_61
node_73:	`node	TY_7,TX_3, node_71,0      ,0      ,node_63
node_74:	`node	TY_7,TX_4, 0      ,node_76,0      ,node_64
node_76:	`node	TY_7,TX_6, node_74,0      ,0      ,node_66
node_77:	`node	TY_7,TX_7, 0      ,node_79,0      ,node_67
node_79:	`node	TY_7,TX_9, node_77,0      ,0      ,node_69


multiply_by_40:
.word	  0,  40,  80, 120, 160
.word	200, 240, 280, 320, 360
.word	400, 440, 480, 520, 560
.word	600, 640, 680, 720, 760
.word	800, 840, 880, 920, 960

item_sizes_by_char:
; bit 7 = dec Y, bit 6 = dec X, bit 0 = tall, bit 1 = wide
.byte	$03,$43,$83,$C3  ; 144..147
.byte	$02,$82          ; 148..149
.byte	$01,$41          ; 150..151
.byte	$03,$43,$83,$C3  ; 152..155


;-----------------------------------------------------------------------

V_WaitBlankStart:
	; require raster line >= 250 and < 256.
	; we load the low byte FIRST, to properly handle the case
	; where it changes from 255 to 256 in-between the loads.

	lda	VIC_RASTER
	ldx	VIC_CTL1
	bmi	V_WaitBlankStart

	cmp	#250
	bcc	V_WaitBlankStart

	rts


V_WaitBlankEnd:
	; require raster line < 128
	lda	VIC_CTL1
	bmi	V_WaitBlankEnd

	lda	VIC_RASTER
	bmi	V_WaitBlankEnd

	rts


.macro	convX	; X reg, dest reg, MSB mask
	lda	_1
	sta	R1

	lda	_1+1
	clc
	adc	#12
	sta	R1+1

	`asl16	R1
	bcc	_msb

	lda	MAZE_1
	ora	#_3
	sta	MAZE_1
_msb:
	lda	R1+1
	sta	_2
.macend


.macro	convY	; Y reg, dest reg
	lda	_1
	sta	R1
	lda	_1+1
	sta	R1+1

	`asl16	R1

	lda	R1+1
	clc
	adc	#74
	sta	_2
.macend


V_PositionActors:
	; reset XMSB to zero
	lda	#0
	sta	MAZE_1

	`convX	PAC+_X, VIC_SP_X+0, $01
	`convY	PAC+_Y, VIC_SP_Y+0

	`convX	GHOST1+_X, VIC_SP_X+2, $06
	`convY	GHOST1+_Y, VIC_SP_Y+2
	; duplicate body position for eyes
	lda	VIC_SP_X+2
	sta	VIC_SP_X+4
	lda	VIC_SP_Y+2
	sta	VIC_SP_Y+4

	`convX	GHOST2+_X, VIC_SP_X+6, $18
	`convY	GHOST2+_Y, VIC_SP_Y+6
	; duplicate body position for eyes
	lda	VIC_SP_X+6
	sta	VIC_SP_X+8
	lda	VIC_SP_Y+6
	sta	VIC_SP_Y+8

	`convX	GHOST3+_X, VIC_SP_X+10, $60
	`convY	GHOST3+_Y, VIC_SP_Y+10
	; duplicate body position for eyes
	lda	VIC_SP_X+10
	sta	VIC_SP_X+12
	lda	VIC_SP_Y+10
	sta	VIC_SP_Y+12

	`convX	BONUS+_X, VIC_SP_X+14, $80
	`convY	BONUS+_Y, VIC_SP_Y+14

	lda	MAZE_1
	sta	VIC_SP_XMSB
	rts


V_IncreaseScore:
	; R1 = amount in hexadecimal, upto $9999
	; clobbers: X, Y, A, R5, R6
	ldy	#5
*
	lda	R1
	and	#15
	clc
	adc	SCORE,y

	cmp	#10
	bcc	+

	sec
	sbc	#10
	tax

	`add16	$10,R1

	txa
*
	sta	SCORE,y

	`lsr16	R1
	`lsr16	R1
	`lsr16	R1
	`lsr16	R1

	dey
	bpl	--

	jmp	V_DrawScore


V_DrawScore:
	`stor16	screen_p+0,R6
	`stor16	colors_p+0,R5

	lda	#C_LGRAY
	sta	MAZE_2
	ldy	#12

	lda	SCORE+0
	jsr	V_DrawDigit
	lda	SCORE+1
	jsr	V_DrawDigit
	lda	SCORE+2
	jsr	V_DrawDigit
	lda	SCORE+3
	jsr	V_DrawDigit
	lda	SCORE+4
	jsr	V_DrawDigit
	lda	SCORE+5

V_DrawDigit:
	iny
	iny

	asl
	asl
	clc
	adc	#200

V_DrawBigChar:
	; A = top-left character
	; Y = offset from LHS of screen
	; R6 = beginning of line in screen
	; R5 = beginning of line in colors
	; MAZE_2 = color
	; clobbers: X and A
	tax

	sta	(R6),y
	lda	MAZE_2
	sta	(R5),y

	iny
	sta	(R5),y
	inx
	txa
	sta	(R6),y
	dey

	`add16	40,R6
	`add16	40,R5

	inx
	txa
	sta	(R6),y
	lda	MAZE_2
	sta	(R5),y

	iny
	sta	(R5),y
	inx
	txa
	sta	(R6),y
	dey

	`sub16	40,R6
	`sub16	40,R5
	rts


V_DrawBigChar_Step:
	jsr	V_DrawBigChar
	iny
	iny
	rts


V_DrawReady:
	ldx	#9
*
	lda	ready_message,x
	sta	screen_p+55,x
	lda	#C_WHITE
	sta	colors_p+55,x

	dex
	bpl	-
	rts


V_DrawLevelNum:
	ldx	#4
*
	lda	level_message,x
	sta	screen_p+32,x
	lda	#C_CYAN
	sta	colors_p+32,x

	dex
	bpl	-

	lda	LEVEL
	lsr
	lsr
	lsr
	lsr
	ora	#$30  ; '0'
	sta	screen_p+38

	lda	LEVEL
	and	#15
	ora	#$30
	sta	screen_p+39

	lda	#C_CYAN
	sta	colors_p+38
	sta	colors_p+39
	rts


ready_message:
.byte	"GET READY!"

level_message:
.byte	"LEVEL"


V_DrawLives:
	`stor16	screen_p+0,R6
	`stor16	colors_p+0,R5

	lda	#C_YELLOW
	sta	MAZE_2
	ldy	#0

	lda	#0
	sta	R8	; current life (0..4)
	lda	#240
	sta	R8+1	; big character
*
	lda	R8
	cmp	LIVES
	bcc	+

	lda	#0
	sta	R8+1
*
	lda	R8+1
	jsr	V_DrawBigChar

	iny
	iny
	inc	R8

	cpy	#10
	bmi	--
	rts


V_DrawHexWord:
	; R1 = word
	; R6 = screen pointer (which is moved)
	; clobbers: A, Y

	lda	R1+1
	jsr	V_DrawHex
	lda	R1

V_DrawHex:
	; A = value
	; R6 = screen pointer (which is moved)
	; clobbers: A, Y

	pha
	lsr
	lsr
	lsr
	lsr

	jsr	V_DrawHexDigit
	pla

V_DrawHexDigit:
	and	#$0F
	tay
	lda	hex_digits,y
	ldy	#0
	sta	(R6),y

	`inc16	R6
	rts


hex_digits:
.byte	"0123456789ABCDEF"


; --- editor settings ----
; vi:ts=12:sw=12:noexpandtab
