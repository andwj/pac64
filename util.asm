;===============================================
;
;     PACMAN CLONE for the C=64
;
;     by Andrew Apted, July 2019
;
;===============================================


; --- memory stuff -----

U_FillMem:
	; R1 = start address
	; R2 = length
	; A = fill value
	; clobbers : A, X, Y
	; NOTE : this is very very slow

	tax
	ldy	#0
*
	lda	R2+1
	ora	R2
	beq	u_filldone

	txa
	sta	(R1),y

	`inc16	R1
	`dec16	R2

	jmp	-

u_filldone:
	rts


U_CopyMem:
	; R1 = start address
	; R2 = length
	; R3 = destination
	; clobbers : A, Y
	; NOTE : this is very very slow

	ldy	#0
*
	lda	R2+1
	ora	R2
	beq	u_copydone

	lda	(R1),y
	sta	(R3),y

	`inc16	R1
	`inc16	R3
	`dec16	R2

	jmp	-

u_copydone:
	rts


; --- random number generator -----

U_Random:
	; result : A = random byte
	; clobbers : nothing

	stx	Z_RAND+2

	; Z_RAND = index into random tab
	; Z_RAND+1 = stepping (must be odd)
	lda	Z_RAND
	clc
	adc	Z_RAND+1
	sta	Z_RAND

	; when index reaches zero, add 2 to the stepping
	bne	+

	inc	Z_RAND+1
	inc	Z_RAND+1
*
	ldx	Z_RAND
	lda	random_tab,x

	ldx	Z_RAND+2
	rts


U_SeedRandom:
	; get some entropy from:
	;   (a) timer A low byte
	;   (b) raster position

	lda	CIA1_TA_LO
	sta	Z_RAND	; index

	lda	VIC_RASTER
	ora	#1
	sta	Z_RAND+1	; step (must be odd)

	rts


;
; random table is 256 bytes, each value appearing once.
; it was generated from Linux's /dev/random device with
; some missing values added.
;
random_tab:
.byte	$01, $0E, $04, $12, $05, $17, $1F, $B2
.byte	$19, $A9, $E6, $44, $F0, $EA, $B6, $CE
.byte	$77, $55, $D8, $95, $36, $29, $D9, $25
.byte	$67, $FB, $3C, $C9, $35, $6A, $24, $69
.byte	$B8, $43, $8D, $11, $87, $F3, $A4, $3D
.byte	$47, $65, $FC, $CC, $61, $89, $DB, $B3
.byte	$E8, $78, $A2, $DC, $F9, $D2, $B9, $B7
.byte	$C3, $CD, $1E, $F5, $0B, $D7, $5A, $73
.byte	$DA, $96, $A5, $07, $D4, $BC, $13, $16
.byte	$76, $48, $00, $85, $03, $50, $14, $F4
.byte	$28, $AC, $6C, $BF, $8A, $4D, $9D, $53
.byte	$23, $83, $5E, $82, $DF, $C7, $4F, $A0
.byte	$A6, $34, $C1, $9C, $79, $FF, $6D, $98
.byte	$08, $6E, $9E, $60, $FE, $FA, $B0, $A8
.byte	$86, $E2, $CA, $94, $E5, $88, $CF, $51
.byte	$9B, $75, $26, $5D, $54, $E4, $4C, $BB

.byte	$F1, $0C, $5B, $56, $0A, $2E, $1B, $57
.byte	$7E, $46, $45, $D5, $E3, $0D, $6F, $EC
.byte	$42, $DD, $8E, $7F, $5F, $1A, $8F, $41
.byte	$22, $91, $93, $F2, $F7, $59, $C0, $39
.byte	$3B, $66, $81, $06, $38, $37, $AF, $AD
.byte	$BA, $E0, $4E, $D6, $0F, $E1, $E9, $97
.byte	$4A, $3F, $CB, $6B, $EB, $ED, $7B, $A1
.byte	$58, $C4, $B4, $30, $9A, $02, $DE, $27
.byte	$52, $BE, $EF, $2F, $E7, $F8, $1D, $72
.byte	$15, $21, $C6, $49, $AE, $33, $20, $C8
.byte	$8C, $9F, $68, $31, $D3, $10, $FD, $4B
.byte	$09, $92, $C2, $AA, $A7, $64, $EE, $1C
.byte	$AB, $2A, $18, $D0, $32, $74, $B5, $90
.byte	$BD, $F6, $5C, $99, $D1, $2B, $B1, $63
.byte	$8B, $3A, $84, $3E, $7D, $2C, $40, $70
.byte	$7C, $62, $C5, $2D, $71, $A3, $7A, $80


U_RandomDir:
	; A = non-zero mask of possible dirs
	; result: A = random dir
	; clobbers: R1
	sta	R1
	sty	R1+1

	jsr	U_Random

	and	#7
	asl
	asl
	asl
	asl
	ora	R1

	tay
	lda	random_dir_table,y

	ldy	R1+1
	rts

random_dir_table:
.byte	0,LF,RT,LF,DN,LF,RT,LF, UP,LF,RT,LF,DN,LF,RT,LF
.byte	0,LF,RT,RT,DN,DN,DN,RT, UP,UP,UP,RT,UP,DN,DN,RT
.byte	0,LF,RT,LF,DN,LF,RT,DN, UP,LF,RT,UP,DN,UP,UP,DN
.byte	0,LF,RT,RT,DN,DN,DN,LF, UP,UP,UP,LF,UP,LF,RT,UP
.byte	0,LF,RT,LF,DN,LF,RT,RT, UP,LF,RT,RT,DN,DN,DN,LF
.byte	0,LF,RT,RT,DN,DN,DN,DN, UP,UP,UP,UP,UP,UP,UP,RT
.byte	0,LF,RT,LF,DN,LF,RT,RT, UP,LF,RT,LF,DN,LF,RT,DN
.byte	0,LF,RT,RT,DN,DN,DN,DN, UP,UP,UP,UP,UP,DN,UP,UP

opposite_dir_table:
.byte	0,RT,LF,LF,UP,UP,UP,UP, DN,DN,DN,DN,LF,RT,LF,RT

single_dir_table:
.byte	0,LF,RT,LF,DN,DN,DN,DN, UP,UP,UP,UP,UP,UP,UP,UP


U_SelectDirFromDelta:
	; R1+0 is X delta (a single byte)
	; R1+1 is Y delta (ditto)
	; R4 is bitmask of possible dirs
	; output: A = new dir
	; clobbers: Y, R4, TEMP_Y

	; only a single dir is possible?
	ldy	R4
	lda	single_dir_table,y
	cmp	R4
	bne	+
	rts
*
	; handle the no-turning cases
	lda	R4
	cmp	#LF+RT
	bne	+
	jmp	U_SelectDir_LF_RT
*
	lda	R4
	cmp	#UP+DN
	bne	+
	jmp	U_SelectDir_UP_DN
*
	; compute: ABS(dx) and ABS(dy)
	lda	R1+1
	bpl	+
	`negate
*
	sta	TEMP_Y

	lda	R1+0
	bpl	+
	`negate
*
	; make horizontal dirs win in a tie
	clc
	adc	#2

	; see if ABS(dx) < ABS(dy)
	cmp	TEMP_Y
	bcc	U_SelectDir_Vert


U_SelectDir_Horiz:
	; remove horiz direction facing AWAY
	lda	R1
	bpl	+

	lda	R4
	and	#255-RT
	sta	R4

	; choose left if possible (since dx < 0)
	and	#LF
	beq	++
	rts
*
	lda	R4
	and	#255-LF
	sta	R4

	; choose right if possible (since dx > 0)
	and	#RT
	beq	+
	rts
*
	; LF and RT are both gone, restart process
	jmp	U_SelectDirFromDelta


U_SelectDir_Vert:
	; remove vert direction facing AWAY
	lda	R1+1
	bpl	+

	lda	R4
	and	#255-DN
	sta	R4

	; choose up if possible (since dy < 0)
	and	#UP
	beq	++
	rts
*
	lda	R4
	and	#255-UP
	sta	R4

	; choose down if possible (since dy > 0)
	and	#DN
	beq	+
	rts
*
	; UP and DN are both gone, restart process
	jmp	U_SelectDirFromDelta


U_SelectDir_LF_RT:
	lda	R1
	beq	++
	bpl	+

	lda	#LF
	rts
*
	lda	#RT
	rts
*
	; delta is zero, so pick randomly
	lda	#LF+RT
	jmp	U_RandomDir


U_SelectDir_UP_DN:
	lda	R1+1
	beq	++
	bpl	+

	lda	#UP
	rts
*
	lda	#DN
	rts
*
	; delta is zero, so pick randomly
	lda	#UP+DN
	jmp	U_RandomDir


; --- numeric conversions -----

U_HexDigit:
	; A = 0 to 15  (can be higher, is masked)
	; result: A = '0'..'9' or 'A'..'F'

	and	#15
	cmp	#10
	bcc	+

	clc
	adc	#55
	rts
*
	clc
	adc	#48
	rts


number_buffer:
	.byte	0,0,0,0, 0,0,0,0


U_HexByte:
	; A = byte to convert
	; X = offset into number_buffer
	;
	; result: X = new offset, buffer updated

	pha

	lsr
	lsr
	lsr
	lsr

	jsr	U_HexDigit
	sta	number_buffer,x
	inx

	pla

	jsr	U_HexDigit
	sta	number_buffer,x
	inx

	; ensure buffer is terminated
	lda	#0
	sta	number_buffer,x

	rts


U_HexWord:
	; R2 = value (16 bits)
	; X = offset into number_buffer

	lda	R2+1
	jsr	U_HexByte

	lda	R2
	jsr	U_HexByte

	rts


DecimalDigit:
	; R2 = unsigned value (16 bits)
	; R1 = a multiple of 10 (e.g. 10000, 1000, 100)
	; X  = offset into number_buffer

	ldy	#0

	; WHILE R2 >= R1 do
	;   R2 = R2 - R1
	;   y  = y + 1
	; WEND

u_decdigit_loop:
	lda	R2+1
	cmp	R1+1
	bcc	u_decdigit_done

	bne	+

	lda	R2
	cmp	R1
	bcc	u_decdigit_done
*
	lda	R2
	sec
	sbc	R1
	sta	R2

	lda	R2+1
	sbc	R1+1
	sta	R2+1

	iny
	bne	u_decdigit_loop

u_decdigit_done:
	tya
	clc
	adc	#'0

	sta	number_buffer,x
	inx

	rts


U_Decimal:
	; R2 = unsigned value (16 bits)
	; X = offset into number_buffer
	;
	; result: 5 digits in number_buffer
	; clobbers: R1, R2, X, Y
	;
	; NOTE: when using 4/3/2 digit versions, the input
	;       value *MUST* be in the corresponding range
	;       (e.g. for 3 digits, must be 0..999)

	`stor16	10000, R1
	jsr	DecimalDigit

U_Decimal4:
	`stor16	1000, R1
	jsr	DecimalDigit

U_Decimal3:
	`stor16	100, R1
	jsr	DecimalDigit

U_Decimal2:
	`stor16	10, R1
	jsr	DecimalDigit

	`stor16	1, R1
	jsr	DecimalDigit

	lda	#0
	sta	number_buffer,x

	rts


; --- editor settings ----
; vi:ts=12:sw=12:noexpandtab
