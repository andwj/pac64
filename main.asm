;===============================================
;
;     PACMAN CLONE for the C=64
;
;     by Andrew Apted, July 2019
;
;===============================================


.require	"defs.asm"


;---- PRG header --------------

	.word	$0801	; load address of basic programs
	.org	$0801

	.word	line_20	; link to next line of program
	.word	10	; line number
	.byte	$8f	; "REM" token
	.byte	" ---- PAC64 ----",0
line_20:
	.word	line_30	; link to next line of program
	.word	10	; line number
	.byte	$8f	; "REM" token
	.byte	" BY ANDREW APTED",0
line_30:
	.word	prg_end	; link to next line of program
	.word	10	; line number
	.byte	$9E	; "SYS" token
	.byte	" 2112",0
prg_end:
	.word	0	; link (0 = end of basic program)

	.advance	2112


;---- main program --------------

Main:
	; disable interrupts while we setup the hardware
	sei

	; clear binary-coded decimal mode
	cld

	; clear break flag
	php
	pla
	and	#$EF
	pha
	plp

	; reset stack pointer
	ldx     #$FF
	txs

	; clear low memory
	ldx	#2
	lda	#0
*
	sta	$0,x
	inx
	bne	-

	; seed RNG now to grab some entropy sources
	; (like current raster position)
	jsr	U_SeedRandom

	jsr	I_InitInput
	jsr	S_InitSound
	jsr	V_InitVideo

	; NOTE: we don't use interrupts, so we keep them disabled

	lda	#GS_WAIT
	sta	GAME_STATE

	jsr	V_DrawTitle

	lda	#0
	sta	VIC_SP_ENA

G_Loop:
	jsr	V_WaitBlankStart

	; collision register is cleared after a read, so save it now
	lda	VIC_HIT_SP
	sta	COLLISION
	lda	VIC_HIT_BG
	sta	COLLISION+1
	lda	#0
	sta	HIT_GHOST

	jsr	G_GameAction

	jsr	V_PositionActors
	jsr	V_WaitBlankEnd

	jsr	I_UpdateInput
	jsr	S_UpdateSound

	`inc16	FRAME

	lda	GAME_STATE
	cmp	#GS_ACTIVE
	bne	G_Loop

	jsr	G_State_ActiveNonBlank
	jmp	G_Loop


G_NewGame:
	lda	#$01
	sta	LEVEL
	lda	#3
	sta	LIVES

	lda	#0
	sta	SCORE+0
	sta	SCORE+1
	sta	SCORE+2
	sta	SCORE+3
	sta	SCORE+4
	sta	SCORE+5

	jsr	G_InitMaze
	rts


G_InitMaze:
	lda	#0
	sta	DOTNUM  ; updated by V_DrawMaze

	`stor16	1,FRAME

	jsr	V_ClearScreen

	jsr	V_DrawMaze
	jsr	V_DrawLives
	jsr	V_DrawLevelNum
	jsr	V_DrawReady

	jsr	G_InitPaccie
	jsr	G_InitBonus
	jsr	G_InitGhosts

	jsr	V_WaitBlankStart

	lda	#255
	sta	VIC_SP_ENA
	rts


G_GameAction:
	lda	GAME_STATE
	cmp	#GS_ACTIVE
	bne	+
	jmp	G_State_Active
*
	cmp	#GS_FINISH
	bne	+
	jmp	G_State_Finish
*
	cmp	#GS_DYING
	bne	+
	jmp	G_State_Dying
*
	cmp	#GS_OVER
	bne	+
	jmp	G_State_Over
*
	cmp	#GS_MUSIC
	bne	G_State_Wait
	jmp	G_State_Music


G_State_Wait:
	lda	IN_FIRE
	bmi	+
	rts
*
	jsr	G_NewGame

	lda	#GS_MUSIC
	sta	GAME_STATE
	lda	#75 ; about 5.0 seconds
	sta	GAME_TIME

	jsr	G_StartIntroMusic
	rts


G_StartIntroMusic:
	lda	#0
	sta	CHAN_TIME+S_VOICE1
	sta	CHAN_TIME+S_VOICE2
	`stor16	intro_music, CHAN_PTR+S_VOICE1
	`stor16	intro_music_bg, CHAN_PTR+S_VOICE2

	; give background channel a head start
	lda #5
	sta CHAN_TIME+S_VOICE1
	rts


G_State_Music:
	lda	FRAME
	and	#3
	beq	+
	rts
*
	dec	GAME_TIME
	beq	+
	rts
*
	lda	#GS_ACTIVE
	sta	GAME_STATE
	rts


G_State_Active:
	jsr	G_UpdatePaccie
	jsr	G_UpdateGhosts
	jsr	G_UpdatePower
	jmp	G_UpdateBonus


G_State_ActiveNonBlank:
	jsr	G_TryEatDot
	jsr	G_TryEatBonus
	jsr	G_TryEatGhost

	ldx	#GHOST1
	jsr	G_GhostLook
	ldx	#GHOST2
	jsr	G_GhostLook
	ldx	#GHOST3
	jsr	G_GhostLook

	jmp	G_BumpTravelDist


G_FinishLevel:
	lda	#4
	sta	CHAN_TIME+S_VOICE2
	lda	#0
	sta	CHAN_TIME+S_VOICE3
	`stor16	finished_music,CHAN_PTR+S_VOICE2
	`stor16	finished_music_bg,CHAN_PTR+S_VOICE3

	lda	#GS_FINISH
	sta	GAME_STATE
	lda	#40
	sta	GAME_TIME
	rts


G_State_Finish:
	lda	FRAME
	and	#3
	beq	+
	rts
*
	dec	GAME_TIME
	beq	+
	rts
*
	; increase level number (using BCD mode)
	lda	LEVEL
	sed
	clc
	adc	#$01
	sta	LEVEL
	cld

	; this will draw the new level number
	jsr	G_InitMaze

	lda	#GS_ACTIVE
	sta	GAME_STATE
	rts


G_PaccieDie:
	; play the dying effect
	lda	#0
	sta	CHAN_TIME+S_VOICE1
	`stor16	die_sfx, CHAN_PTR+S_VOICE1

	lda	#SP_PAC_UP
	sta	PAC+_ANIM
	sta	spr_pointers+0

	lda	#GS_DYING
	sta	GAME_STATE
	lda	#DIE_TIME
	sta	GAME_TIME

	rts


G_State_Dying:
	lda	FRAME
	and	#3
	beq	+
	rts
*
	; animate player sprite
	lda	FRAME
	and	#12
	bne	+

	lda	PAC+_ANIM
	cmp	#SP_PAC_DIE+5
	beq	+

	inc	PAC+_ANIM
	lda	PAC+_ANIM
	sta	spr_pointers+0
*
	dec	GAME_TIME
	beq	+
	rts
*
	; subtract a life
	lda	LIVES
	beq	G_GameOver

	dec	LIVES
	jsr	V_DrawLives

	jsr	G_InitPaccie
	jsr	G_InitGhosts

	lda	#GS_ACTIVE
	sta	GAME_STATE
	rts


G_GameOver:
	lda	#GS_OVER
	sta	GAME_STATE

	lda	#$3F  ; around 4 seconds
	sta	GAME_TIME
	rts


G_State_Over:
	; draw game over text, then wait for fire button

	lda	FRAME
	and	#3
	beq	+
	rts
*
	dec	GAME_TIME
	bne	+

	lda	#GS_WAIT
	sta	GAME_STATE
	rts
*
	; draw the "GAME OVER" letters one by one.

	lda	GAME_TIME
	eor	#$3F
	lsr
	lsr

	cmp	#9
	bcs	+

	tax
	lda	game_over_message,x
	sta	screen_p+40*15+16,x
	lda	#C_WHITE
	sta	colors_p+40*15+16,x
*
	rts


;------------------------------------------------------------------------

G_InitPaccie:
	`stor16	TX_5*256,PAC+_X
	`stor16	[TY_5+6]*256,PAC+_Y
	`stor16	node_55,PAC+_FROM
	`stor16	node_65,PAC+_DEST

	lda	#0
	sta	PAC+_FLAGS
	sta	PAC+_ANIM

	lda	#BASE_SPEED
	sta	PAC+_SPEED
	lda	#0
	sta	PAC+_DIR

	lda	#SP_PAC_SOLID
	sta	spr_pointers+0

	lda	#C_YELLOW
	sta	VIC_SP_COL+0

	rts


G_InitGhosts:
	ldx	#GHOST1
	jsr	G_InitGhostFromDB
	ldx	#GHOST2
	jsr	G_InitGhostFromDB
	ldx	#GHOST3
	jsr	G_InitGhostFromDB

	`stor16	node_35,GHOST1+_FROM
	`stor16	node_35,GHOST2+_FROM
	`stor16	node_35,GHOST3+_FROM

	; this dest won't apply until ghost leaves the pen
	`stor16	node_45,GHOST1+_DEST
	`stor16	node_45,GHOST2+_DEST
	`stor16	node_45,GHOST3+_DEST

	lda	#SP_GHOST_BODY
	sta	spr_pointers+1
	sta	spr_pointers+3
	sta	spr_pointers+5

	lda	#SP_EYES_DOWN
	sta	spr_pointers+2
	sta	spr_pointers+4
	sta	spr_pointers+6

	lda	ghost1_database+DB_COLOR
	sta	VIC_SP_COL+1
	lda	ghost2_database+DB_COLOR
	sta	VIC_SP_COL+3
	lda	ghost3_database+DB_COLOR
	sta	VIC_SP_COL+5

	lda	#0
	sta	VIC_SP_PRI

	; the eyes use MultiColor mode
	lda	#%01010100
	sta	VIC_SP_MCM

	lda	#C_BLUE
	sta	VIC_SP_MC1
	lda	#C_WHITE
	sta	VIC_SP_MC2

	lda	ghost1_database+DB_MOUTH
	sta	VIC_SP_COL+2
	lda	ghost2_database+DB_MOUTH
	sta	VIC_SP_COL+4
	lda	ghost3_database+DB_MOUTH
	sta	VIC_SP_COL+6

	rts


G_InitGhostFromDB:
	; X = actor address
	lda	#FL_IN_PEN
	sta	_FLAGS,x

	lda	ghost1_database-GHOST1+DB_SPEED,x
	sta	_SPEED,x

	; start coordinate
	lda	#0
	sta	_X+0,x
	sta	_Y+0,x
	lda	ghost1_database-GHOST1+DB_PEN_X,x
	sta	_X+1,x
	lda	#ghost_pen_ty
	sta	_Y+1,x

	; ghosts initially go to their home quadrant
	lda	ghost1_database-GHOST1+DB_QUAD+0,x
	sta	_TARGET+0,x
	lda	ghost1_database-GHOST1+DB_QUAD+1,x
	sta	_TARGET+1,x

	; time to leave the pen
	lda	ghost1_database-GHOST1+DB_PEN_TIME,x
	sta	_ANIM,x

	lda	#DN
	sta	_DIR,x

	rts


G_UpdatePaccie:
	jsr	G_MovePaccie

	ldx	#0
	lda	PAC+_DIR
	bne	+

	rts
*
	tax
	lda	dir_to_anim_table,x
	asl
	clc
	adc	#SP_PAC_LEFT

	tax
	lda	#8
	bit	FRAME+0
	beq	+
	inx
*
	stx	spr_pointers+0
	rts


G_UpdateGhosts:
	ldx	#GHOST1
	jsr	G_MoveActor
	ldx	#GHOST2
	jsr	G_MoveActor
	ldx	#GHOST3
	jsr	G_MoveActor

	ldx	#SP_GHOST_BODY
	lda	#16
	bit	FRAME+0
	beq	+
	inx
*
	stx	spr_pointers+1
	stx	spr_pointers+3
	stx	spr_pointers+5

	; eye directions

	ldx	GHOST1+_DIR
	lda	dir_to_anim_table,x
	clc
	adc	#SP_EYES_LEFT
	sta	spr_pointers+2

	ldx	GHOST2+_DIR
	lda	dir_to_anim_table,x
	clc
	adc	#SP_EYES_LEFT
	sta	spr_pointers+4

	ldx	GHOST3+_DIR
	lda	dir_to_anim_table,x
	clc
	adc	#SP_EYES_LEFT
	sta	spr_pointers+6

	rts


dir_to_anim_table:
.byte	0,0,1,1, 2,2,2,2, 3,3,3,3, 3,3,3,3



G_MovePaccie:
	ldx	#PAC

	lda	PAC+_DIR
	beq	+
	jmp	G_MovePaccieReal
*
	lda	spr_pointers+0
	cmp	#SP_PAC_SOLID
	beq	G_MovePaccieFromStart

	; see which directions are possible
	`mov16	PAC+_FROM,R3

	; down?
	ldy	#7
	lda	(R3),y
	beq	+

	lda	#DN
	bit	IN_DIR
	beq	+

	jmp	G_SetNewDir
*
	; up?
	ldy	#9
	lda	(R3),y
	beq	+

	lda	#UP
	bit	IN_DIR
	beq	+

	jmp	G_SetNewDir
*
	; left?
	ldy	#3
	lda	(R3),y
	beq	+

	lda	#LF
	bit	IN_DIR
	beq	+

	jmp	G_SetNewDir
*
	; right?
	ldy	#5
	lda	(R3),y
	beq	+

	lda	#RT
	bit	IN_DIR
	beq	+

	jmp	G_SetNewDir
*
	; cannot move
	rts


G_MovePaccieFromStart:
	; Paccie is a motionless ball, check to start moving
	lda	#DN
	bit	IN_DIR
	beq	+

	sta	PAC+_DIR
	rts
*
	lda	#UP
	bit	IN_DIR
	bne	+

	rts
*
	lda	#DN
	sta	PAC+_DIR
	; this takes care of the node pointers
	jmp	G_ReverseDirection


G_MovePaccieReal:
	sta	TRAV_DIR

	; check if player wants to reverse dir
	lda	TRAV_DIST
	cmp	#3
	bcc	G_MoveActor

	ldy	PAC+_DIR
	lda	opposite_dir_table,y
	bit	IN_DIR
	beq	G_MoveActor

	jmp	G_ReverseDirection


G_MoveActor:
	; X is the actor base e.g. GHOST1
	; clobbers: A, X, Y, R3, R4
	cpx	#PAC
	beq	G_MoveActor2

	lda	_FLAGS,x
	bmi	G_MoveActor2

	and	#FL_IN_PEN
	beq	+

	jmp	G_GhostInPen
*
	; check if ghost collided with paccie
	jsr	G_CheckHitGhost

G_MoveActor2:
	lda	_DEST,x
	sta	R3
	lda	_DEST+1,x
	sta	R3+1

	lda	_DIR,x
	cmp	#RT
	bne	+
	jmp	G_MoveActorRight
*
	cmp	#DN
	bne	+
	jmp	G_MoveActorDown
*
	cmp	#LF
	beq	G_MoveActorLeft
	jmp	G_MoveActorUp

G_MoveActorLeft:
	lda	_X,x
	sec
	sbc	_SPEED,x
	sta	_X,x
	bcs	+

	dec	_X+1,x

	; reached dest node?
	lda	_X+1,x
	ldy	#1
	cmp	(R3),y
	bcs	+

	jmp	G_ReachedNode
*
	rts

G_MoveActorRight:
	lda	_X,x
	clc
	adc	_SPEED,x
	sta	_X,x
	bcc	+

	inc	_X+1,x

	; reached dest node?
	lda	_X+1,x
	ldy	#1
	cmp	(R3),y
	bcc	+

	jmp	G_ReachedNode
*
	rts

G_MoveActorDown:
	lda	_Y,x
	clc
	adc	_SPEED,x
	sta	_Y,x
	bcc	+

	inc	_Y+1,x

	; reached dest node?
	lda	_Y+1,x
	ldy	#0
	cmp	(R3),y
	bcc	+

	jmp	G_ReachedNode
*
	rts

G_MoveActorUp:
	lda	_Y,x
	sec
	sbc	_SPEED,x
	sta	_Y,x
	bcs	+

	dec	_Y+1,x

	; reached dest node?
	lda	_Y+1,x
	ldy	#0
	cmp	(R3),y
	bcs	+

	jmp	G_ReachedNode
*
	rts


G_ReachedNode:
	; snap coordinate
	lda	#0
	sta	_X,x
	sta	_Y,x

	ldy	#0
	lda	(R3),y
	sta	_Y+1,x
	iny
	lda	(R3),y
	sta	_X+1,x

	; check for dead ghost reaching the pen
	cpx	#PAC
	beq	+

	lda	_FLAGS,x
	and	#FL_GO_PEN
	beq	+

	lda	_X+1,x
	cmp	#ghost_pen_tx
	bne	+

	lda	_Y+1,x
	cmp	#ghost_pen_ty
	bne	+

	jmp	G_GhostReachPen
*
	; build bitmask of available dirs
	lda	#0
	sta	R4

	ldy	#3
	lda	(R3),y
	beq	+

	inc	R4  ; set #LF
*
	iny
	iny
	lda	(R3),y
	beq	+

	lda	R4
	ora	#RT
	sta	R4
*
	iny
	iny
	lda	(R3),y
	beq	+

	lda	R4
	ora	#DN
	sta	R4
*
	iny
	iny
	lda	(R3),y
	beq	+

	lda	R4
	ora	#UP
	sta	R4
*
	; disallow direction into ghost pen
	lda	R3
	cmp	#<node_45
	bne	++

	lda	R3+1
	cmp	#>node_45
	bne	++

	cpx	#PAC
	beq	+

	; allow dead ghosts to enter the pen
	lda	_FLAGS,x
	bmi	++
*
	lda	R4
	and	#255-UP
	sta	R4
*
	; remove opposite of current travel dir
	ldy	_DIR,x
	lda	opposite_dir_table,y
	eor	#255
	and	R4
	bne	+

	; no dirs left -- must be a dead end
	jmp	G_ReverseDirection
*
	sta	R4

	cpx	#PAC
	bne	G_GhostNavigate

G_PaccieNavigate:
	; A = possible dirs

	; can we travel in joystick/keyboard direction?
	bit	IN_DIR
	beq	+

	and	IN_DIR

	; if pressing diagonally (two dirs), pick one.
	; [ this table favors up and down ]
	tay
	lda	single_dir_table,y

	jmp	G_SetNewDir
*
	; can we keep going in current direction?
	bit	PAC+_DIR
	beq	G_StopPaccie

	lda	PAC+_DIR
	jmp	G_SetNewDir

G_StopPaccie:
	lda	#0
	sta	PAC+_DIR

	`mov16	PAC+_DEST,PAC+_FROM
	rts


G_GhostNavigate:
	; R4 = possible dirs

	ldy	_TARGET+1,x
	bne	G_GhostGotoTarget

	lda	_CHASE,x
	beq	G_GhostWander

	jmp	G_GhostChasePaccie


G_GhostWander:
	; wander randomly
	lda	R4
	jsr	U_RandomDir
	jmp	G_SetNewDir


G_GhostGotoTarget:
	; reached target?
	lda	_DEST,x
	cmp	_TARGET,x
	bne	G_GhostAimForTarget

	lda	_DEST+1,x
	cmp	_TARGET+1,x
	bne	G_GhostAimForTarget

	; yes
	; [ note: returning was handled earlier ]

	; is ghost dead?
	lda	_FLAGS,x
	bmi	+

	; alive: clear target and go looking for Paccie
	lda	#0
	sta	_TARGET,x
	sta	_TARGET+1,x
	jmp	G_GhostWander
*
	; dead: set target to return to the pen
	lda	#<node_35
	sta	_TARGET,x
	lda	#>node_35
	sta	_TARGET+1,x

	lda	_FLAGS,x
	ora	#FL_GO_PEN
	sta	_FLAGS,x
	jmp	G_GhostAimForTarget


G_GhostAimForTarget:
	; occasionally pick a random dir
	jsr	U_Random
	and	#7
	bne	+

	jmp	G_GhostWander
*
	; compute: target_tx - current_x
	lda	_TARGET,x
	sta	R2
	lda	_TARGET+1,x
	sta	R2+1

	lda	_X+1,x
	lsr
	sta	TEMP_A

	ldy	#1
	lda	(R2),y
	lsr
	sec
	sbc	TEMP_A
	sta	R1

	; compute: target_ty - current_y
	lda	_Y+1,x
	lsr
	sta	TEMP_A

	ldy	#0
	lda	(R2),y
	lsr
	sec
	sbc	TEMP_A
	sta	R1+1

	jsr	U_SelectDirFromDelta
	jmp	G_SetNewDir


G_GhostChasePaccie:
	; compute delta coordinates
	lda	_X+1,x
	lsr
	sta	TEMP_A

	lda	PAC+_X+1
	lsr
	sec
	sbc	TEMP_A
	sta	R1

	lda	_Y+1,x
	lsr
	sta	TEMP_A

	lda	PAC+_Y+1
	lsr
	sec
	sbc	TEMP_A
	sta	R1+1

	; flee instead?
	lda	_FLAGS,x
	and	#FL_SCARED
	beq	+

	lda	R1
	`negate
	sta	R1

	lda	R1+1
	`negate
	sta	R1+1
*
	; hacky fix for ghosts circling around their pen when
	; Paccie is in a certain area underneath (near start).
	lda	_Y+1,x
	cmp	#TY_4
	bne	+

	jsr	G_PacInBadSpot
	bcc	+

	lda	R1+1
	adc	#8
	sta	R1+1
*
	jsr	U_SelectDirFromDelta
	jmp	G_SetNewDir


G_PacInBadSpot:
	lda	PAC+_Y+1
	cmp	#TY_5
	bcc	+

	cmp	#TY_5+8
	bcs	+

	lda	PAC+_X+1
	cmp	#TX_3+2
	bcc	+

	cmp	#TX_7-1
	bcs	+

	sec
	rts
*
	clc
	rts


G_SetNewDir:
	sta	_DIR,x

	; update node info
	lda	_DEST,x
	sta	_FROM,x
	lda	_DEST+1,x
	sta	_FROM+1,x

	jsr	G_NextNodeOffset

	lda	(R3),y
	sta	_DEST,x
	iny
	lda	(R3),y
	sta	_DEST+1,x

	; OK!
	rts


G_ReverseDirection:
	ldy	_DIR,x
	lda	opposite_dir_table,y
	sta	_DIR,x

	lda	_FROM,x
	pha
	lda	_DEST,x
	sta	_FROM,x
	pla
	sta	_DEST,x

	lda	_FROM+1,x
	pha
	lda	_DEST+1,x
	sta	_FROM+1,x
	pla
	sta	_DEST+1,x
	rts


G_NextNodeOffset:
	lda	_DIR,x
	cmp	#LF
	bne	+
	ldy	#2
	rts
*
	cmp	#RT
	bne	+
	ldy	#4
	rts
*
	cmp	#DN
	bne	+
	ldy	#6
	rts
*
	ldy	#8
	rts


G_GhostInPen:
	dec	_ANIM,x
	beq	+
	rts
*
	; leave the pen!
	lda	_FLAGS,x
	and	#255-FL_IN_PEN
	sta	_FLAGS,x

	; fix X coordinate
	lda	#0
	sta	_X,x
	lda	#ghost_pen_tx
	sta	_X+1,x
	rts


G_GhostReachPen:
	lda	#FL_IN_PEN
	sta	_FLAGS,x

	lda	#0
	sta	_X+0,x
	sta	_Y+0,x
	lda	ghost1_database-GHOST1+DB_PEN_X,x
	sta	_X+1,x
	lda	#ghost_pen_ty
	sta	_Y+1,x

	lda	#DN
	sta	_DIR,x

	lda	#250
	sta	_ANIM,x

	lda	ghost1_database-GHOST1+DB_SPEED,x
	sta	_SPEED,x

	lda	#0
	sta	_TARGET,x
	sta	_TARGET+1,x

	lda	ghost1_database-GHOST1+DB_COLOR,x
	ldy	ghost1_database-GHOST1+DB_MOUTH,x
	jsr	G_SetGhostColors

	cpx	#GHOST3
	beq	++
	cpx	#GHOST2
	beq	+

	; GHOST1
	lda	VIC_SP_PRI
	and	#%11111101
	sta	VIC_SP_PRI

	`stor16	node_35,GHOST1+_FROM
	`stor16	node_45,GHOST1+_DEST

	rts
*
	; GHOST2
	lda	VIC_SP_PRI
	and	#%11110111
	sta	VIC_SP_PRI

	`stor16	node_35,GHOST2+_FROM
	`stor16	node_45,GHOST2+_DEST

	rts
*
	; GHOST 3
	lda	VIC_SP_PRI
	and	#%11011111
	sta	VIC_SP_PRI

	`stor16	node_35,GHOST3+_FROM
	`stor16	node_45,GHOST3+_DEST

	rts


G_GhostLook:
	; countdown current chase
	lda	FRAME
	and	#7
	bne	+

	lda	_CHASE,x
	beq	+
	dec	_CHASE,x
*
	; look ahead to see if Paccie is in sight
	; if yes, set _CHASE to XXX
	; if not, and _CHASE > 0, decrement it
	lda	_FLAGS,x
	and	#FL_DEAD+FL_IN_PEN+FL_SCARED
	beq	+
	rts
*
	lda	_TARGET+1,x
	beq	+
	rts
*
	; wall underneath pen always blocks sight downwards
	lda	_X+1,x
	cmp	#TX_3+2
	bcc	+
	cmp	#TX_7-2
	bcs	+

	lda	PAC+_X+1
	cmp	#TX_3+2
	bcc	+
	cmp	#TX_7-2
	bcs	+

	lda	_Y+1,x
	cmp	#TY_4+6
	bcs	+

	lda	PAC+_Y+1
	cmp	#TY_4+6
	bcc	+

	rts
*
	lda	_DIR,x
	and	#UP+DN
	bne	G_GhostLook_Vert

G_GhostLook_Horiz:
	; same row (roughly) ?
	lda	_Y+1,x
	sec
	sbc	#3
	cmp	PAC+_Y+1
	bcc	+
	rts
*
	lda	_Y+1,x
	clc
	adc	#3
	cmp	PAC+_Y+1
	bcs	+
	rts
*
	lda	_X+1,x
	ldy	_DIR,x
	cpy	#RT
	beq	+

	cmp	PAC+_X+1
	bcs	G_GhostLook_See
	rts
*
	cmp	PAC+_X+1
	bcc	G_GhostLook_See
	rts


G_GhostLook_Vert:
	; same column (roughly) ?
	lda	_X+1,x
	sec
	sbc	#3
	cmp	PAC+_X+1
	bcc	+
	rts
*
	lda	_X+1,x
	clc
	adc	#3
	cmp	PAC+_X+1
	bcs	+
	rts
*
	lda	_Y+1,x
	ldy	_DIR,x
	cpy	#DN
	beq	+

	cmp	PAC+_Y+1
	bcs	G_GhostLook_See
	rts
*
	cmp	PAC+_Y+1
	bcc	G_GhostLook_See
	rts

G_GhostLook_See:
	lda	#CHASE_TIME
	sta	_CHASE,x

	rts


G_BumpTravelDist:
	lda	TRAV_DIR
	cmp	PAC+_DIR
	beq	+

	; direction changed, reset TRAV_DIST so that it becomes
	; negative in roughly 0.5 seconds.
	lda	#0
	sta	TRAV_DIST
	rts
*
	bit	TRAV_DIST
	bmi	+
	inc	TRAV_DIST
*
	rts


G_TryEatDot:
	; check VIC-II collision with background
	; [ player sprite never overlaps anything else ]
	lda	COLLISION+1
	and	#1
	bne	+
	rts
*
	; determine screen cell from Paccie's location
	`mov16	PAC+_X,R1
	`mov16	PAC+_Y,R2

	jsr	V_CheckDotOrPellet

	lda	R3
	cmp	#2
	beq	G_EatPellet

	cmp	#1
	beq	G_EatDot
	rts

G_EatDot:
	; play sound
	lda	#0
	sta	CHAN_TIME+S_VOICE1
	`stor16	munch_sfx,CHAN_PTR+S_VOICE1

	; add to score
	`stor16	$0010,R1
	jsr	V_IncreaseScore

	lda	DOTNUM
	bne	+

	jmp	G_FinishLevel
*
	; check if time for a ghost to go home
	jmp	G_GhostsGoHome


G_EatPellet:
	; I.... have... the power!!!

	lda	#0
	sta	CHAN_TIME+S_VOICE3
	`stor16	pellet_sfx,CHAN_PTR+S_VOICE3

	; add to score
	`stor16	$0050,R1
	jsr	V_IncreaseScore

	lda	DOTNUM
	bne	+

	jmp	G_FinishLevel
*
	lda	#0
	sta	EATEN

	; the power time gets shorter on later levels
	lda	LEVEL
	cmp	#$20
	bcc	+
	lda	#$20
*
	asl
	sta	TEMP_A

	lda	#POWER_TIME
	sec
	sbc	TEMP_A
	sta	POWERUP

	ldx	#GHOST1
	jsr	G_ScareGhost
	ldx	#GHOST2
	jsr	G_ScareGhost
	ldx	#GHOST3
	jmp	G_ScareGhost


G_UpdatePower:
	lda	POWERUP
	bne	+
	rts
*
	lda	FRAME
	and	#7
	beq	+
	rts
*
	dec	POWERUP
	beq	G_LostPower

	; flash ghosts white/blue when near finished
	lda	POWERUP
	and	#$E0
	beq	+
	rts
*
	ldx	#GHOST1
	jsr	G_FlashGhost
	ldx	#GHOST2
	jsr	G_FlashGhost
	ldx	#GHOST3
	jsr	G_FlashGhost
	rts


G_LostPower:
	; the magic has gone
	ldx	#GHOST1
	jsr	G_UnscareGhost
	ldx	#GHOST2
	jsr	G_UnscareGhost
	ldx	#GHOST3
	jmp	G_UnscareGhost


G_ScareGhost:
	; ghosts in their pen are not affected, as well as
	; eaten ghosts and already scared ghosts.
	lda	_FLAGS,x
	and	#FL_IN_PEN+FL_DEAD+FL_SCARED
	beq	+
	rts
*
	lda	_FLAGS,x
	ora	#FL_SCARED
	sta	_FLAGS,x

	; change direction?
	jsr	G_CheckReverseGhost

	; enable chase mode, but ghost will FLEE paccie
	lda	#255
	sta	_CHASE,x

	lda	#SLOW_SPEED
	sta	_SPEED,x

	lda	#C_BLUE
	ldy	#C_ORANGE

G_SetGhostColors:
	cpx	#GHOST1
	bne	+

	sta	VIC_SP_COL+1
	sty	VIC_SP_COL+2
	rts
*
	cpx	#GHOST2
	bne	+

	sta	VIC_SP_COL+3
	sty	VIC_SP_COL+4
	rts
*
	sta	VIC_SP_COL+5
	sty	VIC_SP_COL+6
	rts


G_CheckReverseGhost:
	lda	_DIR,x

	cmp	#UP
	beq	G_CheckReverse_UP
	cmp	#LF
	beq	G_CheckReverse_LF
	cmp	#RT
	beq	G_CheckReverse_RT

G_CheckReverse_DN:
	lda	_Y+1,x
	cmp	PAC+_Y+1
	bcs	+
	jmp	G_ReverseDirection
*
	rts

G_CheckReverse_UP:
	lda	_Y+1,x
	cmp	PAC+_Y+1
	bcc	+
	jmp	G_ReverseDirection
*
	rts

G_CheckReverse_LF:
	lda	_X+1,x
	cmp	PAC+_X+1
	bcc	+
	jmp	G_ReverseDirection
*
	rts

G_CheckReverse_RT:
	lda	_X+1,x
	cmp	PAC+_X+1
	bcs	+
	jmp	G_ReverseDirection
*
	rts


G_UnscareGhost:
	lda	_FLAGS,x
	and	#FL_SCARED
	bne	+
	rts
*
	lda	_FLAGS,x
	and	#255-FL_SCARED
	sta	_FLAGS,x

	lda	#0
	sta	_CHASE,x

	lda	ghost1_database-GHOST1+DB_SPEED,x
	sta	_SPEED,x

	; restore colors
	lda	ghost1_database-GHOST1+DB_COLOR,x
	ldy	ghost1_database-GHOST1+DB_MOUTH,x

	jmp	G_SetGhostColors


G_FlashGhost:
	lda	_FLAGS,x
	and	#FL_SCARED
	bne	+
	rts
*
	cpx	#GHOST1
	bne	+

	lda	VIC_SP_COL+1
	jsr	G_FlashGhost_Color
	sta	VIC_SP_COL+1
	rts
*
	cpx	#GHOST2
	bne	+

	lda	VIC_SP_COL+3
	jsr	G_FlashGhost_Color
	sta	VIC_SP_COL+3
	rts
*
	lda	VIC_SP_COL+5
	jsr	G_FlashGhost_Color
	sta	VIC_SP_COL+5
	rts

G_FlashGhost_Color:
	and	#15
	cmp	#C_BLUE
	bne	+

	lda	#C_WHITE
	rts
*
	lda	#C_BLUE
	rts


G_GhostsGoHome:
	; a dot was eaten, check if time to go home
	ldx	#GHOST1
	jsr	G_GhostCheckGoHome
	ldx	#GHOST2
	jsr	G_GhostCheckGoHome
	ldx	#GHOST3

G_GhostCheckGoHome:
	lda	ghost1_database-GHOST1+DB_HOME_DOT,x
	cmp	DOTNUM
	beq	+
	rts
*
	; occasionally don't
	jsr	U_Random
	and	#$E0
	bne	+

	lda	#100
	sta	_CHASE,x
	rts
*
	; too busy?
	; [ note that we allow FL_IN_PEN here ]
	lda	_FLAGS,x
	and	#FL_DEAD+FL_SCARED
	beq	+
	rts
*
	lda	ghost1_database-GHOST1+DB_QUAD+0,x
	sta	_TARGET+0,x
	lda	ghost1_database-GHOST1+DB_QUAD+1,x
	sta	_TARGET+1,x

	lda	#0
	sta	_CHASE,x
	rts


G_CheckHitGhost:
	; X is the GHOST1/2/3 struct

	lda	_FLAGS,x
	and	#FL_DEAD+FL_IN_PEN
	beq	+
	rts
*
	; check both VIC-II collision and distance
	lda	#%00000011
	cpx	#GHOST1
	beq	+

	lda	#%00001001
	cpx	#GHOST2
	beq	+

	lda	#%00100001
*
	sta	TEMP_A

	lda	COLLISION
	and	TEMP_A
	cmp	TEMP_A
	bne	+

	jsr	V_IsTouching
	bcc	+

	; collision!
	stx	HIT_GHOST
*
	rts


G_TryEatGhost:
	ldx	HIT_GHOST
	bne	+
	rts
*
	; if ghost is not scared, Paccie is dead
	lda	_FLAGS,x
	and	#FL_SCARED
	bne	G_EatGhost

	jmp	G_PaccieDie


G_EatGhost:
	lda	_FLAGS,x
	ora	#FL_DEAD
	and	#255-FL_SCARED
	sta	_FLAGS,x

	lda	#0
	sta	_CHASE,x

	lda	#BASE_SPEED
	sta	_SPEED,x

	; send ghost to its home quadrant
	; [ after it gets there it goes back to pen ]
	lda	ghost1_database-GHOST1+DB_QUAD+0,x
	sta	_TARGET+0,x
	lda	ghost1_database-GHOST1+DB_QUAD+1,x
	sta	_TARGET+1,x

	; add to score
	lda	EATEN
	and	#3
	asl
	tay

	lda	ghost_scores,y
	sta	R1
	lda	ghost_scores+1,y
	sta	R1+1

	txa
	pha

	jsr	V_IncreaseScore

	pla
	tax

	inc	EATEN

	; play sound
	lda	#0
	sta	CHAN_TIME+S_VOICE2
	`stor16	ghost_sfx,CHAN_PTR+S_VOICE2

	; turn body "invisible" (really just black)
	lda	#C_BLACK
	ldy	#C_BLACK
	jsr	G_SetGhostColors

	cpx	#GHOST1
	bne	+

	lda	VIC_SP_PRI
	ora	#%00000010
	sta	VIC_SP_PRI
	rts
*
	cpx	#GHOST2
	bne	+

	lda	VIC_SP_PRI
	ora	#%00001000
	sta	VIC_SP_PRI
	rts
*
	lda	VIC_SP_PRI
	ora	#%00100000
	sta	VIC_SP_PRI
	rts


; entries here must be $10 bytes, same as (GHOST2 - GHOST1).
ghost1_database:
.byte C_RED, C_BROWN, BASE_SPEED+2
.byte ghost_pen_tx, 8
.byte 70, 0, 0
.word node_79, 0,0,0

ghost2_database:
.byte C_PINK, C_BROWN, BASE_SPEED-16
.byte ghost_pen_tx-6, 32
.byte 30, 0, 0
.word node_15, 0,0,0

ghost3_database:
.byte C_GREEN, C_BROWN, BASE_SPEED-40
.byte ghost_pen_tx+6, 56
.byte 50, 0, 0
.word node_71, 0,0,0


;------------------------------------------------------------------------

G_InitBonus:
	`stor16	bon_start_tx*256,BONUS+_X
	`stor16	bon_start_ty*256,BONUS+_Y

	lda	#0
	sta	BONUS+_FLAGS

	; time to appear: 8-17 seconds
	jsr	U_Random
	ora	#128
	sta	BONUS+_ANIM

	lda	#SP_BLANK
	sta	spr_pointers+7

	lda	#C_RED
	sta	VIC_SP_COL+7

	rts


G_UpdateBonus:
	; already eaten or disappeared?
	lda	BONUS+_FLAGS
	bpl	+
	rts
*
	; decrement counter (every 4th frame)
	lda	FRAME+0
	and	#3
	beq	+
	rts
*
	dec	BONUS+_ANIM

	; visible?
	lda	BONUS+_FLAGS
	and	#1
	bne	G_BonusIsVisible

	; time to make it visible?
	lda	BONUS+_ANIM
	beq	+
	rts
*
	; choose graphic and score based on current level
	lda	LEVEL
	and	#$0F  ; low digit
	asl
	tax

	lda	level_bonuses,x
	sta	spr_pointers+7
	lda	level_bonuses+1,x
	sta	VIC_SP_COL+7

	lda	bonus_scores,x
	sta	BON_SCORE
	lda	bonus_scores+1,x
	sta	BON_SCORE+1

	; how long to hang around: 8-17 seconds
	jsr	U_Random
	ora	#128
	sta	BONUS+_ANIM

	lda	#1
	sta	BONUS+_FLAGS
	rts


G_BonusIsVisible:
	; time to make it disappear?
	lda	BONUS+_ANIM
	bne	+

	; ok, make it vanish
	lda	#SP_BLANK
	sta	spr_pointers+7

	lda	#FL_DEAD
	sta	BONUS+_FLAGS
*
	rts


G_TryEatBonus:
	; is Paccie touching the bonus item?
	; NOTE: we check both VIC-II collision and distance
	lda	COLLISION
	and	#$81
	cmp	#$81
	bne	+

	ldx	#BONUS
	jsr	V_IsTouching
	bcs	G_EatBonus
*
	rts


G_EatBonus:
	lda	#SP_BLANK
	sta	spr_pointers+7

	lda	#FL_DEAD
	sta	BONUS+_FLAGS

	; add to score
	`mov16	BON_SCORE,R1
	jsr	V_IncreaseScore

	; play sound
	lda	#0
	sta	CHAN_TIME+S_VOICE3
	`stor16	bonus_sfx,CHAN_PTR+S_VOICE3

	rts


level_bonuses:
.byte	SP_BONUS_TRUCK,  C_YELLOW  ; 0
.byte	SP_BONUS_CHERRY, C_RED     ; 1
.byte	SP_BONUS_ORANGE, C_PINK    ; 2
.byte	SP_BONUS_COFFEE, C_LGRAY   ; 3
.byte	SP_BONUS_CHERRY, C_RED     ; 4
.byte	SP_BONUS_KEY,    C_YELLOW  ; 5
.byte	SP_BONUS_ORANGE, C_GREEN   ; 6
.byte	SP_BONUS_BIRD,   C_LBLUE   ; 7
.byte	SP_BONUS_CHERRY, C_RED     ; 8
.byte	SP_BONUS_ORANGE, C_MAGENTA ; 9

bonus_scores:
.word	$2500, $0150, $0250, $0350, $0150
.word	$1000, $0450, $1500, $0150, $0650

ghost_scores:
.word	$0300, $0600, $1200, $1800


;---- code modules --------------

.require	"util.asm"
.require	"input.asm"
.require	"video.asm"
.require	"sound.asm"

.require	"chars.asm"
.require	"graphics.asm"


; --- editor settings ----
; vi:ts=12:sw=12:noexpandtab
