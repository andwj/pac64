;===============================================
;
;     PACMAN CLONE for the C=64
;
;     by Andrew Apted, July 2019
;
;===============================================


; hardware registers

.alias	IO_DDR	$00	; DDR for next register
.alias	IO_ROM_CTL	$01	; has cassette I/O too

.alias	CIA1_PRA	$DC00	; port A: keyboard col / joy2
.alias	CIA1_PRB	$DC01	; port B: keyboard row / joy1
.alias	CIA1_DDRA	$DC02	; data direction
.alias	CIA1_DDRB	$DC03	;
.alias	CIA1_ICR	$DC0D	; interrupt control

.alias	CIA1_TA_LO	$DC04	; timer A latch
.alias	CIA1_TA_HI	$DC05	;
.alias	CIA1_TIMA	$DC0E	; timer A control
.alias	CIA1_TB_LO	$DC06	; timer B latch
.alias	CIA1_TB_HI	$DC07	;
.alias	CIA1_TIMB	$DC0F	; timer B control

.alias	CIA2_PRA	$DD00	; port A: VIC bank / RS232 / disk
.alias	CIA2_PRB	$DD01	; port B: userport / RS232
.alias	CIA2_DDRA	$DD02	; data direction
.alias	CIA2_DDRB	$DD03	;
.alias	CIA2_ICR	$DD0D	; interrupt control

.alias	CIA2_TA_LO	$DD04	; timer A latch
.alias	CIA2_TA_HI	$DD05	;
.alias	CIA2_TIMA	$DD0E	; timer A control
.alias	CIA2_TB_LO	$DD06	; timer B latch
.alias	CIA2_TB_HI	$DD07	;
.alias	CIA2_TIMB	$DD0F	; timer B control


; the bank numbers are reverse of what you would expect!
.alias	BANK_0000	3
.alias	BANK_4000	2
.alias	BANK_8000	1
.alias	BANK_C000	0


; CPU interrupt vectors

.alias	NMI_Vector	$FFFA
.alias	Rst_Vector	$FFFC
.alias	IRQ_Vector	$FFFE


;
; we turn off the KERNAL and BASIC roms here so we can handle all
; the interupts ourselves.  Note we can write to the RAM underneath
; even while the roms are still active.
;
I_InitInput:
	; NOTE: video code sets up BANK_XXX in CIA2_PRA

	; reset DDR registers in CIA chips
	lda	#0
	sta	CIA1_DDRA
	sta	CIA1_DDRB

	lda	#%00111111
	sta	CIA2_DDRA
	lda	#0
	sta	CIA2_DDRB

	; reset interrupts in CIA chips
	lda	CIA1_ICR
	lda	CIA2_ICR
	lda	#0
	sta	CIA1_ICR
	sta	CIA2_ICR

	; reset timers in CIA chips
	lda	#0
	sta	CIA1_TIMA
	sta	CIA1_TIMB
	sta	CIA2_TIMA
	sta	CIA2_TIMB

	; disable VIC-II interrupts
	lda	#0
	sta	VIC_INT_ENA

	`stor16	NMI_Handler,   NMI_Vector
	`stor16	Reset_Handler, Rst_Vector
	`stor16	IRQ_Handler,   IRQ_Vector

	; reset DDR register at address $0.
	; bit 4 is cassette switch input, everything else is output.
	lda	#%11101111
	sta	IO_DDR

	; our custom vectors are in place, so kill the roms.
	; bit $20 turns the cassette motor OFF.
	; bit $04 keeps the I/O registers mapped at $D000-$DFFF.
	; bit $01 is necessary too (otherwise we see RAM at $D000).
	lda	#%00100101
	sta	IO_ROM_CTL

	rts


NMI_Handler:
	; we don't use NMI, so ignore it
	rti


Reset_Handler:
	sei

	; restore KERNAL and BASIC roms
	lda	#%00100111
	sta	IO_ROM_CTL

	; enter the original handler
	jmp	$FCE2


IRQ_Handler:
	; we don't use any IRQs, so ignore it
	rti


I_UpdateInput:
	lda	#0
	sta	IN_DIR
	sta	IN_FIRE
	sta	IN_SHIFT

	jsr	I_ReadKeyMatrix
	jsr	I_ReadJoysticks

	; don't process keyboard when a joystick is in use, since
	; one of the joysticks interferes with the keyboad matrix.
	lda	IN_DIR
	ora	IN_FIRE
	bne	+

	jsr	I_ProcessKeyboard
*
	; cancel out opposing directions
	ldx	IN_DIR
	lda	joy_cancellation,x
	sta	IN_DIR

	rts


I_ReadKeyMatrix:
	lda	#$FF
	sta	CIA1_DDRA
	lda	#0
	sta	CIA1_DDRB

	ldx	#7
*
	; set active keyboard column
	lda	bit_flags,x
	eor	#$FF
	sta	CIA1_PRA

	nop
	nop

	; read keyboard row
	lda	CIA1_PRB
	eor	#$FF
	sta	KEY_MATRIX,x

	dex
	bpl	-

	; reset active column to zero
	lda	#0
	sta	CIA1_PRA

	rts


I_ReadJoysticks:
	lda	#0
	sta	CIA1_DDRA
	sta	CIA1_DDRB

	; we combine values of both joysticks
	lda	CIA1_PRA
	and	CIA1_PRB
	and	#$0F
	eor	#$0F

	tax
	lda	joy_mapping,x
	ora	IN_DIR
	sta	IN_DIR

	lda	CIA1_PRA
	and	CIA1_PRB
	and	#$10
	bne	+

	dec	IN_FIRE
*
	rts


I_ProcessKeyboard:
	; handle SHIFT key
	lda	KEY_MATRIX+1
	and	#$80
	beq	+

	dec	IN_SHIFT
*
	lda	KEY_MATRIX+6
	and	#$10
	beq	+

	dec	IN_SHIFT
*
	; handle UP/DOWN key
	lda	KEY_MATRIX+0
	and	#$80
	beq	++

	lda	#DN
	bit	IN_SHIFT
	bpl	+

	lda	#UP
*
	ora	IN_DIR
	sta	IN_DIR
*
	; handle LEFT/RIGHT key
	lda	KEY_MATRIX+0
	and	#$04
	beq	++

	lda	#RT
	bit	IN_SHIFT
	bpl	+

	lda	#LF
*
	ora	IN_DIR
	sta	IN_DIR
*
	; handle SPACE and RETURN keys
	lda	KEY_MATRIX+7
	and	#$10
	beq	+

	dec	IN_FIRE
*
	lda	KEY_MATRIX+0
	and	#$02
	beq	+

	dec	IN_FIRE
*
	; handle W/A/S/D
	lda	KEY_MATRIX+1
	and	#$02
	beq	+

	lda	#UP
	ora	IN_DIR
	sta	IN_DIR
*
	lda	KEY_MATRIX+1
	and	#$04
	beq	+

	lda	#LF
	ora	IN_DIR
	sta	IN_DIR
*
	lda	KEY_MATRIX+1
	and	#$20
	beq	+

	lda	#DN
	ora	IN_DIR
	sta	IN_DIR
*
	lda	KEY_MATRIX+2
	and	#$04
	beq	+

	lda	#RT
	ora	IN_DIR
	sta	IN_DIR
*
	rts


bit_flags:
.byte	$01,$02,$04,$08,$10,$20,$40,$80

joy_mapping:
.byte	$0,UP,DN,UP+DN
.byte	LF,LF+UP,LF+DN,LF+UP+DN
.byte	RT,RT+UP,RT+DN,RT+UP+DN
.byte	RT+LF,RT+LF+UP,RT+LF+DN,RT+LF+UP+DN

joy_cancellation:
.byte	$00,$01,$02,$00,$04,$05,$06,$04
.byte	$08,$09,$0A,$08,$00,$01,$02,$00


; --- editor settings ----
; vi:ts=12:sw=12:noexpandtab
