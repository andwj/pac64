;===============================================
;
;     PACMAN CLONE for the C=64
;
;     by Andrew Apted, July 2019
;
;===============================================


;---- global constants --------

; zero page registers

.alias	TEMP_A	$02	; very temporary save for A
.alias	TEMP_X	$03	; very temporary save for X
.alias	TEMP_Y	$04	; very temporary save for Y

.alias	Z_TEST	$08	; 8 bytes for testing stuff
			; (not used in normal code)

.alias	R1	$10	;
.alias	R2	$12	;
.alias	R3	$14	;
.alias	R4	$16	; fake registers, each 16 bits
.alias	R5	$18	;
.alias	R6	$1A	;
.alias	R7	$1C	;
.alias	R8	$1E	;

.alias	Z_RAND	$20	; 3 bytes for the RNG

.alias	MAZE_1	$24
.alias	MAZE_2	$25

.alias	FRAME	$26	; word, incremented every frame

.alias	LEVEL	$28	; current level (01..99, BCD)
.alias	LIVES	$29	; number of lives left
.alias	SCORE	$2A	; six digits for current score

.alias	GAME_STATE	$30	; game state, one of GS_XXX values
.alias	GAME_TIME	$31	; counts down during certain game states
.alias	DOTNUM	$32	; number of dots remaining
.alias	POWERUP	$33	; counts down while pellet is active

.alias	IN_DIR	$34	; input direction from KB and joysticks
.alias	IN_FIRE	$35	; input fire buttons (bit 7 set if pressed)
.alias	IN_SHIFT	$36	; input SHIFT key (bit 7 set if pressed)
.alias	KEY_MATRIX	$38	; 8 bytes : pressed keys

.alias	TRAV_DIST	$40	; # of frames since Paccie changed dir
.alias	TRAV_DIR	$41	; Paccie's travel dir in last frame
.alias	COLLISION	$42	; saved VIC-II sprite collision regs (two bytes)
.alias	HIT_GHOST	$44	; usually zero, or GHOST1/2/3 if hit a ghost
.alias	EATEN	$45	; how many ghosts eaten during power trip
.alias	BON_SCORE	$46	; score for current bonus


; state of sound system
; S_VOICE# are constants representing a SID voice, must be 7 apart.
; CHAN_XX are variables for for each voice, and mirrors some data
; written to the SID chip (since we cannot read back those values).
.alias	S_VOICE1	$0
.alias	S_VOICE2	$7
.alias	S_VOICE3	$E

.alias	CHAN_PTR	$60	; pointer for sfx/music
.alias	CHAN_TIME	$62	; time remaining on current note
.alias	CHAN_WAVE	$64	; current waveform
.alias	CHAN_FREQ	$76	; current frequency
.alias	CHAN_DELTA	$78	; delta frequency

; format of sound effects and music is a sequence of commands.
; each command begins with a single byte and may be followed
; by some extra data (arguments to the command).
; the SET_XX commands happen instantiously, but the NOTE_XX
; commands wait for the given number of screen frames.
.alias	NOTE_ON	$F1	; length | note
.alias	NOTE_GLIDE	$F2	; length | note | delta L | delta H
.alias	NOTE_OFF	$F3	; length
.alias	GLIDE_OFF	$F4	;
.alias	SET_WAVE	$F6	; waveform | pulse
.alias	SET_ENV	$F7	; atk+dcy | sus+rel
.alias	END_SFX	$FF	; marks end of sfx/music


; state for the five actors (Paccie + 3 ghosts + bonus).
; each actor has:
;   - flags (byte)
;   - animation (byte)
;   - travel dir (byte)
;   - travel speed (byte)
;   - travel X (word, range is 0 to 40*4*256)
;   - travel Y (word, range is 0 to 25*4*256)
;   - from node (pointer)
;   - dest node (pointer)
.alias	PAC	$A0
.alias	GHOST1	$B0
.alias	GHOST2	$C0
.alias	GHOST3	$D0
.alias	BONUS	$E0

.alias	_FLAGS	$0
.alias	_ANIM	$1
.alias	_DIR	$2
.alias	_SPEED	$3
.alias	_CHASE	$4
.alias	_X	$6
.alias	_Y	$8
.alias	_FROM	$A
.alias	_DEST	$C
.alias	_TARGET	$E

; actor flags

.alias	FL_DEAD	$80
.alias	FL_IN_PEN	$40
.alias	FL_SCARED	$20
.alias	FL_GO_PEN	$10

; misc constants
.alias	POWER_TIME	80  ; around 10 seconds
.alias	CHASE_TIME  60  ; around 8 seconds
.alias	DIE_TIME	60  ; around 4.5 seconds

; speed of Paccie, ghosts are based on this
.alias	BASE_SPEED	144
.alias	SLOW_SPEED	BASE_SPEED/2

; ghost database fields
.alias	DB_COLOR	$0
.alias	DB_MOUTH	$1
.alias	DB_SPEED	$2
.alias	DB_PEN_X	$3
.alias	DB_PEN_TIME	$4
.alias	DB_HOME_DOT	$5
.alias	DB_QUAD	$8  ; home quadrant


; location of screen and charset (for VIC-II)

.alias	screen_p	$7000
.alias	colors_p	$D800
.alias	charset_p	$7800
.alias	sprites_p	$5000

.alias	spr_pointers   screen_p + $3F8


; directions

.alias	LF	$01	; left
.alias	RT	$02	; right
.alias	DN	$04	; down
.alias	UP	$08	; up

; colors

.alias	C_BLACK	0
.alias	C_WHITE	1
.alias	C_RED	2
.alias	C_CYAN	3
.alias	C_MAGENTA	4
.alias	C_GREEN	5
.alias	C_BLUE	6
.alias	C_YELLOW	7

.alias	C_ORANGE	8
.alias	C_BROWN	9
.alias	C_PINK	10
.alias	C_DGRAY	11
.alias	C_MGRAY	12
.alias	C_LGREEN	13
.alias	C_LBLUE	14
.alias	C_LGRAY	15

; sprites

.alias	SP_BLANK		64
.alias	SP_PAC_SOLID	65
.alias	SP_PAC_LEFT		66
.alias	SP_PAC_RIGHT	68
.alias	SP_PAC_DOWN		70
.alias	SP_PAC_UP		72
.alias	SP_PAC_DIE		74

.alias	SP_GHOST_BODY	80
.alias	SP_EYES_LEFT	82
.alias	SP_EYES_RIGHT	83
.alias	SP_EYES_DOWN	84
.alias	SP_EYES_UP		85

.alias	SP_BONUS_CHERRY	86
.alias	SP_BONUS_ORANGE	87
.alias	SP_BONUS_COFFEE	88
.alias	SP_BONUS_KEY	89
.alias	SP_BONUS_BIRD	90
.alias	SP_BONUS_TRUCK	91


; game states

.alias	GS_WAIT	0  ; waiting to start
.alias	GS_MUSIC	1  ; playing the start music
.alias	GS_ACTIVE	2  ; game is active (normal play)
.alias	GS_FINISH	3  ; level is won, about to go to next
.alias	GS_DYING	4  ; Paccie was eaten, oh no!
.alias	GS_OVER	5  ; game is over


;---- macros -------------------

.macro	negate	; A register
	eor	#255
	clc
	adc	#1
.macend


.macro	stor16	; constant, dest
	lda	#<_1
	sta	_2+0
	lda	#>_1
	sta	_2+1
.macend


.macro	mov16	; src, dest
	lda	_1+0
	sta	_2+0
	lda	_1+1
	sta	_2+1
.macend


.macro	inc16	; addr
	inc	_1
	bne	_done
	inc	_1+1
_done:
.macend


.macro	dec16	; addr
	lda	_1
	bne	_over
	dec	_1+1
_over:
	dec	_1
.macend


.macro	asl16	; addr
	clc
	rol	_1
	rol	_1+1
.macend


.macro	asl24	; addr
	clc
	rol	_1
	rol	_1+1
	rol	_1+2
.macend


.macro	lsr16	; addr
	clc
	ror	_1+1
	ror	_1
.macend


.macro	add16	; constant, dest
	lda	_2
	clc
	adc	#<_1
	sta	_2

	lda	_2+1
	adc	#>_1
	sta	_2+1
.macend


.macro	sub16	; constant, dest
	lda	_2
	sec
	sbc	#<_1
	sta	_2

	lda	_2+1
	sbc	#>_1
	sta	_2+1
.macend


.macro	push16	; addr
	lda	_1+1
	pha
	lda	_1
	pha
.macend


.macro	pop16	; addr
	pla
	sta	_1
	pla
	sta	_1+1
.macend


; --- editor settings ----
; vi:ts=12:sw=12:noexpandtab
