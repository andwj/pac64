
PACMAN CLONE for the C=64
=========================

About
-----

This is a basic Pacman&reg; clone for the Commodore 64.
It does not strive to exactly copy the original game,
and there are numerous differences.  I made it mainly
for nostalgia, since the Commodore 64 was the computer
I grew up with and learnt to program on.


Status
------

The game is complete and fully playable.


How to Play
-----------

Start your favourite Commodore-64 emulator, such as VICE,
then load the game program and run it:

```
LOAD "PAC64.PRG",8
RUN
```

Press the joystick fire button or SPACE to start a new game.
Move Paccie around using the joystick or the WASD keys on
the keyboard.  Using the cursor keys is not recommended
since the Commodore-64 only had two cursor keys and they
can misrepresent the direction you want to go in.

Paccie needs to eat all the dots to complete the level,
while avoid the deadly ghosts.  Eating a blue power pellet
causes the ghosts to become vulnerable for a short time and
Paccie can eat them instead.  A bonus item also appears at
a random time in each level, grab it before it disappears
for some juicy bonus points.


Screenshot
----------

![Pac64 Screenshot](SCREENSHOT.png "")


Author and License
------------------

Andrew Apted, 2019.

The license is MIT-style, a permissive open source license.
See the [LICENSE.md](LICENSE.md) file for the complete text.

