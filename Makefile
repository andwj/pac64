#
# GNU Makefile
#

PROGRAM  = pac64.prg

all: $(PROGRAM)

clean:
	rm -f $(PROGRAM)

run:
	x64 -VICIIdsize -VICIIgamma 3500

$(PROGRAM): *.asm
	ophis main.asm $@

.PHONY: all clean run

#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
