;===============================================
;
;     PACMAN CLONE for the C=64
;
;     by Andrew Apted, July 2019
;
;===============================================


; hardware registers for 6581 SID chip.
; NOTE: that registers are either READ-only or WRITE-only.

; voice 1
.alias	SID_V1_FREQ_LO	$D400
.alias	SID_V1_FREQ_HI	$D401
.alias	SID_V1_PW_LO	$D402
.alias	SID_V1_PW_HI	$D403
.alias	SID_V1_CTL		$D404
.alias	SID_V1_ATK_DCY	$D405
.alias	SID_V1_SUS_REL	$D406

; voice 2
.alias	SID_V2_FREQ_LO	$D407
.alias	SID_V2_FREQ_HI	$D408
.alias	SID_V2_PW_LO	$D409
.alias	SID_V2_PW_HI	$D40A
.alias	SID_V2_CTL		$D40B
.alias	SID_V2_ATK_DCY	$D40C
.alias	SID_V2_SUS_REL	$D40D

; voice 3
.alias	SID_V3_FREQ_LO	$D40E
.alias	SID_V3_FREQ_HI	$D40F
.alias	SID_V3_PW_LO	$D410
.alias	SID_V3_PW_HI	$D411
.alias	SID_V3_CTL		$D412
.alias	SID_V3_ATK_DCY	$D413
.alias	SID_V3_SUS_REL	$D414

; filter control
.alias	SID_FIL_FREQ_LO	$D415
.alias	SID_FIL_FREQ_HI	$D416
.alias	SID_FIL_RES		$D417
.alias	SID_MODE_VOL	$D418

; control register bits in SID_V#_CTL
.alias	WAVE_NOISE		$80
.alias	WAVE_PULSE		$40
.alias	WAVE_SAWTOOTH	$20
.alias	WAVE_TRIANGLE	$10

.alias	SCTL_TEST	$08
.alias	SCTL_RING	$04
.alias	SCTL_SYNC	$02
.alias	SCTL_GATE	$01  ; 1 = attack/decay/sustain, 0 = release

; fitler voice enable in SID_FIL_RES
.alias	V1_FILTER	$01
.alias	V2_FILTER	$02
.alias	V3_FILTER	$04

; filter modes in SID_MODE_VOL
.alias	FILT_OFF	$00
.alias	FILT_HP	$40  ; high pass
.alias	FILT_BP	$20  ; band pass
.alias	FILT_LP	$10  ; low pass
.alias	FILT_NOTCH	$50  ; HP + LP = notch filter
.alias	V3_QUIET	$80

S_InitSound:
	; reset all registers to zero
	lda	#0
	ldx	#0
*
	sta	$D400,x
	inx
	bne	-

	; pump up the volume!
	lda	#15
	sta	SID_MODE_VOL

	; reset channel info
	`stor16	null_sfx, CHAN_PTR+S_VOICE1
	`stor16	null_sfx, CHAN_PTR+S_VOICE2
	`stor16	null_sfx, CHAN_PTR+S_VOICE3

	rts


;-----------------------------------------------------------------------

; musical notes
; NOTE: 'H' represents the sharp symbol
.alias	C0	0*12+0
.alias	CH0	0*12+1
.alias	D0	0*12+2
.alias	DH0	0*12+3
.alias	E0	0*12+4
.alias	F0	0*12+5
.alias	FH0	0*12+6
.alias	G0	0*12+7
.alias	GH0	0*12+8
.alias	A0	0*12+9
.alias	AH0	0*12+10
.alias	B0	0*12+11

.alias	C1	1*12+0
.alias	CH1	1*12+1
.alias	D1	1*12+2
.alias	DH1	1*12+3
.alias	E1	1*12+4
.alias	F1	1*12+5
.alias	FH1	1*12+6
.alias	G1	1*12+7
.alias	GH1	1*12+8
.alias	A1	1*12+9
.alias	AH1	1*12+10
.alias	B1	1*12+11

.alias	C2	2*12+0
.alias	CH2	2*12+1
.alias	D2	2*12+2
.alias	DH2	2*12+3
.alias	E2	2*12+4
.alias	F2	2*12+5
.alias	FH2	2*12+6
.alias	G2	2*12+7
.alias	GH2	2*12+8
.alias	A2	2*12+9
.alias	AH2	2*12+10
.alias	B2	2*12+11

.alias	C3	3*12+0
.alias	CH3	3*12+1
.alias	D3	3*12+2
.alias	DH3	3*12+3
.alias	E3	3*12+4
.alias	F3	3*12+5
.alias	FH3	3*12+6
.alias	G3	3*12+7
.alias	GH3	3*12+8
.alias	A3	3*12+9
.alias	AH3	3*12+10
.alias	B3	3*12+11

.alias	C4	4*12+0
.alias	CH4	4*12+1
.alias	D4	4*12+2
.alias	DH4	4*12+3
.alias	E4	4*12+4
.alias	F4	4*12+5
.alias	FH4	4*12+6
.alias	G4	4*12+7
.alias	GH4	4*12+8
.alias	A4	4*12+9
.alias	AH4	4*12+10
.alias	B4	4*12+11

.alias	C5	5*12+0
.alias	CH5	5*12+1
.alias	D5	5*12+2
.alias	DH5	5*12+3
.alias	E5	5*12+4
.alias	F5	5*12+5
.alias	FH5	5*12+6
.alias	G5	5*12+7
.alias	GH5	5*12+8
.alias	A5	5*12+9
.alias	AH5	5*12+10
.alias	B5	5*12+11

.alias	C6	6*12+0
.alias	CH6	6*12+1
.alias	D6	6*12+2
.alias	DH6	6*12+3
.alias	E6	6*12+4
.alias	F6	6*12+5
.alias	FH6	6*12+6
.alias	G6	6*12+7
.alias	GH6	6*12+8
.alias	A6	6*12+9
.alias	AH6	6*12+10
.alias	B6	6*12+11

.alias	C7	7*12+0
.alias	CH7	7*12+1
.alias	D7	7*12+2
.alias	DH7	7*12+3
.alias	E7	7*12+4
.alias	F7	7*12+5
.alias	FH7	7*12+6
.alias	G7	7*12+7
.alias	GH7	7*12+8
.alias	A7	7*12+9
.alias	AH7	7*12+10


S_NoteFrequency:
	; A = note number
	; output in R1
	; clobbers: A, R2

	stx	R2

	tax
	inx
	txa

	; divide by 12 : result in X, remainder in A
	ldx	#0
*
	cmp	#12
	bcc	+

	sec
	sbc	#12
	inx
	jmp	-
*
	stx	TEMP_X

	; use lookup table with remainder
	asl
	tax

	lda	note_frequencies+0,x
	sta	R1+0
	lda	note_frequencies+1,x
	sta	R1+1

	; divide by power of two based on octave
	lda	TEMP_X
	and	#7
	eor	#7
	beq	++

	tax
*
	`lsr16	R1
	dex
	bne	-
*
	ldx	R2
	rts


; frequencies for musical notes B6 to A#7.
; lower octaves are produced by dividing by a power of two.
note_frequencies:
.word	$8178,$892B,$9153,$99F7,$A31F,$ACD2
.word	$B719,$C1FC,$CD85,$D9BD,$E6B0,$F467


;-----------------------------------------------------------------------

S_UpdateSound:
	ldx	#S_VOICE1
	jsr	S_UpdateChannel

	ldx	#S_VOICE2
	jsr	S_UpdateChannel

	ldx	#S_VOICE3

S_UpdateChannel:
	; X = S_VOICE1/2/3

	; we move the stream pointer to R4, perform current cmd,
	; then move the updated R4 back into the channel.
	lda	CHAN_PTR,x
	sta	R4
	lda	CHAN_PTR+1,x
	sta	R4+1

	jsr	S_PerformCommand

	lda	R4
	sta	CHAN_PTR,x
	lda	R4+1
	sta	CHAN_PTR+1,x

	rts


S_PerformCommand:
	lda	CHAN_TIME,x
	beq	+

	; finish current note
	jsr	S_UpdateFrequency

	dec	CHAN_TIME,x
	rts
*
	ldy	#0
	lda	(R4),y

	cmp	#END_SFX
	bne	+

	; end of stream, nothing to do
	rts
*
	cmp	#NOTE_ON
	bne	+
	jmp	S_NoteOn
*
	cmp	#NOTE_GLIDE
	bne	+
	jmp	S_NoteGlide
*
	cmp	#NOTE_OFF
	bne	+
	jmp	S_NoteOff
*
	cmp	#GLIDE_OFF
	bne	+
	jmp	S_GlideOff
*
	cmp	#SET_WAVE
	bne	+
	jmp	S_SetWave
*
	cmp	#SET_ENV
	bne	+
	jmp	S_SetEnvelope
*
	; oops, unknown command!
	rts


S_UpdateFrequency:
	; do frequency gliding
	lda	CHAN_FREQ,x
	clc
	adc	CHAN_DELTA,x
	sta	CHAN_FREQ,x
	sta	SID_V1_FREQ_LO,x

	lda	CHAN_FREQ+1,x
	adc	CHAN_DELTA+1,x
	sta	CHAN_FREQ+1,x
	sta	SID_V1_FREQ_HI,x

	rts


S_NoteOn:
	jsr	S_NoteCommon

	lda	#0
	sta	CHAN_DELTA,x
	sta	CHAN_DELTA+1,x

	`add16	3,R4
	rts


S_NoteGlide:
	jsr	S_NoteCommon

	ldy	#3
	lda	(R4),y
	sta	CHAN_DELTA,x

	iny
	lda	(R4),y
	sta	CHAN_DELTA+1,x

	`add16	5,R4
	rts


S_NoteCommon:
	ldy	#1
	lda	(R4),y
	sta	CHAN_TIME,x

	; get note
	iny
	lda	(R4),y

	jsr	S_NoteFrequency

	lda	R1
	sta	CHAN_FREQ,x
	sta	SID_V1_FREQ_LO,x

	lda	R1+1
	sta	CHAN_FREQ+1,x
	sta	SID_V1_FREQ_HI,x

	; turn on GATE bit, start attack/decay/release cycle
	lda	CHAN_WAVE,x
	ora	#SCTL_GATE
	sta	SID_V1_CTL,x

	rts


S_NoteOff:
	ldy	#1
	lda	(R4),y
	sta	CHAN_TIME,x

	; turn off GATE bit, start release cycle of envelope
	lda	CHAN_WAVE,x
	and	#$FF-SCTL_GATE
	sta	SID_V1_CTL,x

	`add16	2,R4
	rts


S_GlideOff:
	lda	#0
	sta	CHAN_DELTA,x
	sta	CHAN_DELTA+1,x

	`inc16	R4
	rts


S_SetWave:
	ldy	#1
	lda	(R4),y
	; this will be used on next NOTE_ON or NOTE_GLIDE
	sta	CHAN_WAVE,x

	iny
	lda	(R4),y
	; convert 8-bit value to a 12-bit value
	sta	R2+1
	lda	#0
	sta	R2

	`lsr16	R2
	`lsr16	R2
	`lsr16	R2
	`lsr16	R2

	lda	R2
	sta	SID_V1_PW_LO,x
	lda	R2+1
	sta	SID_V1_PW_HI,x

	`add16	3,R4
	rts


S_SetEnvelope:
	ldy	#1
	lda	(R4),y
	sta	SID_V1_ATK_DCY,x

	iny
	lda	(R4),y
	sta	SID_V1_SUS_REL,x

	`add16	3,R4
	rts


;-----------------------------------------------------------------------

null_sfx:
.byte	END_SFX

test_sfx:
.byte	SET_WAVE,WAVE_PULSE,$81
.byte	SET_ENV,$88,$48
.byte	NOTE_ON,20,C3, NOTE_OFF,10
.byte	NOTE_ON,20,E3, NOTE_OFF,10
.byte	NOTE_ON,20,G3, NOTE_OFF,10
.byte	END_SFX

die_sfx:
.byte	SET_WAVE,WAVE_SAWTOOTH,$21
.byte	SET_ENV,$19,$09
.byte	NOTE_GLIDE,5,B6, $80,$FF, NOTE_OFF,10
.byte	NOTE_GLIDE,5,F6, $80,$FF, NOTE_OFF,10
.byte	NOTE_GLIDE,5,B5, $80,$FF, NOTE_OFF,10
.byte	NOTE_GLIDE,5,F5, $80,$FF, NOTE_OFF,10
.byte	SET_ENV,$1A,$0A
.byte	NOTE_ON,1,G0, NOTE_OFF,0
.byte	END_SFX

munch_sfx:
.byte	SET_WAVE,WAVE_PULSE,$20
.byte	SET_ENV,$26,$06
.byte	NOTE_ON,2,E3
.byte	NOTE_OFF,3
.byte	END_SFX

pellet_sfx:
.byte	SET_WAVE,WAVE_PULSE,$20
.byte	SET_ENV,$1A,$0A
.byte	NOTE_GLIDE,3,G2,0,1
.byte	NOTE_OFF,50
.byte	END_SFX

ghost_sfx:
.byte	SET_WAVE,WAVE_PULSE,$71
.byte	SET_ENV,$1A,$0A
.byte	NOTE_GLIDE,4,G4,170,255, NOTE_OFF,10
.byte	NOTE_GLIDE,4,C4,100,0,   NOTE_OFF,14
.byte	END_SFX

bonus_sfx:
.byte	SET_WAVE,WAVE_SAWTOOTH,$11
.byte	SET_ENV,$38,$08
.byte	NOTE_ON,3,G5, NOTE_OFF,3
.byte	NOTE_ON,3,A5, NOTE_OFF,3
.byte	NOTE_ON,3,B5, NOTE_OFF,3
.byte	NOTE_ON,3,C6, NOTE_OFF,3
.byte	SET_ENV,$3A,$0A
.byte	NOTE_ON,3,D6, NOTE_OFF,15
.byte	END_SFX

finished_music:
.byte	SET_WAVE,WAVE_TRIANGLE,$51
.byte	SET_ENV,$4D,$0A
.byte	NOTE_ON,5,G5, NOTE_OFF,4
.byte	NOTE_ON,5,B5, NOTE_OFF,13
.byte	NOTE_ON,5,A5, NOTE_OFF,4
.byte	NOTE_ON,5,C6, NOTE_OFF,23
.byte	END_SFX

finished_music_bg:
.byte	SET_WAVE,WAVE_TRIANGLE,$51
.byte	SET_ENV,$5C,$09
.byte	NOTE_ON,5,G4, NOTE_OFF,31
.byte	NOTE_ON,5,B4, NOTE_OFF,31
.byte	END_SFX

intro_music:
.byte	SET_WAVE,WAVE_TRIANGLE,$51
.byte	SET_ENV,$1D,$0A

.byte	NOTE_ON,2,B4, NOTE_OFF,4
.byte	NOTE_ON,2,B5, NOTE_OFF,4
.byte	NOTE_ON,2,FH5, NOTE_OFF,4
.byte	NOTE_ON,2,DH5, NOTE_OFF,4
.byte	NOTE_ON,2,B5,  NOTE_OFF,4
.byte	NOTE_ON,2,FH5, NOTE_OFF,4
.byte	NOTE_ON,2,DH5, NOTE_OFF,12

.byte	NOTE_ON,2,2+B4, NOTE_OFF,4
.byte	NOTE_ON,2,2+B5, NOTE_OFF,4
.byte	NOTE_ON,2,2+FH5, NOTE_OFF,4
.byte	NOTE_ON,2,2+DH5, NOTE_OFF,4
.byte	NOTE_ON,2,2+B5,  NOTE_OFF,4
.byte	NOTE_ON,2,2+FH5, NOTE_OFF,4
.byte	NOTE_ON,2,2+DH5, NOTE_OFF,12

.byte	NOTE_ON,2,B4, NOTE_OFF,4
.byte	NOTE_ON,2,B5, NOTE_OFF,4
.byte	NOTE_ON,2,FH5, NOTE_OFF,4
.byte	NOTE_ON,2,DH5, NOTE_OFF,4
.byte	NOTE_ON,2,B5,  NOTE_OFF,4
.byte	NOTE_ON,2,FH5, NOTE_OFF,4
.byte	NOTE_ON,2,DH5, NOTE_OFF,12

.byte	SET_ENV,$1A,$0A
.byte	NOTE_GLIDE,2,D5,60,0, NOTE_OFF,18
.byte	NOTE_GLIDE,2,F5,60,0, NOTE_OFF,18
.byte	NOTE_GLIDE,2,G5,60,0, NOTE_OFF,18
.byte	NOTE_ON,4,B5, NOTE_OFF,24

.byte	END_SFX

intro_music_bg:
.byte	SET_WAVE,WAVE_TRIANGLE,$11
.byte	SET_ENV,$1C,$0C

.byte	NOTE_ON,4,B3, NOTE_OFF,60
.byte	NOTE_ON,4,2+B3, NOTE_OFF,60
.byte	NOTE_ON,4,4+B3, NOTE_OFF,60

.byte	END_SFX


; --- editor settings ----
; vi:ts=12:sw=12:noexpandtab
